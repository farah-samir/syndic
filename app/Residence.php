<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Residence extends Model
{
    //
    public function actualites()
    {
        return $this->hasMany(Actualite::class);
    }
}
