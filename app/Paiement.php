<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paiement extends Model
{
    //
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public $fillable = ['mois','cotisation', 'etat'];
    
}
