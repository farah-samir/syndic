<?php

namespace App\Exports;

use App\Ville;
use Maatwebsite\Excel\Concerns\FromCollection;

class VilleExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Ville::all();
    }
}
