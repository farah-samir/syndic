<?php

namespace App\Exports;

use App\Residence;
use Maatwebsite\Excel\Concerns\FromCollection;

class ResidenceExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Residence::all();
    }
}
