<?php

namespace App\Exports;

use App\Paiement;
use Maatwebsite\Excel\Concerns\FromCollection;

class PaiementExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Paiement::all();
    }
}