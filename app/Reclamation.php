<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reclamation extends Model
{
    //
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
