<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actualite extends Model
{
    //
    public function residence()
    {
        return $this->belongsTo(Residence::class);
    }
}
