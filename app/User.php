<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements MustVerifyEmail

{
    use Notifiable;

    use SoftDeletes;
    protected $dates = [ 'deleted_at' ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'genre', 'name', 'email', 'password', 'prenom', 'telephone', 'titreFoncier', 'cin', 'ville', 'residence', 'adresse', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function reclamations()
    {
        return $this->hasMany(Reclamation::class);
    }
    
    public function paiements()
    {
        return $this->hasMany('App\Paiement');
    }
    
    public function demandes()
    {
        return $this->hasMany(Demande::class);
    }
    
}
