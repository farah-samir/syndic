<?php

namespace App\Http\Controllers;

use App\Residence;
use App\Ville;

use App\Exports\ResidenceExport;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Maatwebsite\Excel\Facades\Excel;

class ResidenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ville = Ville::latest()->paginate(50);
        $residence = Residence::latest()->paginate(5);

        //return view('ville.index',compact('ville'));

        
        return view('residence.index',compact('residence', 'ville'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $residence = new Residence();

        $residence->nomResidence = $request->input('nomResidence');
        $residence->ville = $request->input('ville');
        $residence->quartie = $request->input('quartie');
        $residence->type = $request->input('type');
        $residence->nbrAppEtage = $request->input('nbrEtage');
        $residence->nbrImmo = $request->input('nbrImmo');
        $residence->nbrEntre = $request->input('nbrEntre');
        $residence->espaceVert = $request->input('espaceVert');
        $residence->nbrAscenseur = $request->input('nbrAscenseur');
        $residence->nbrPark = $request->input('nbrPark');
        $residence->annee = $request->input('annee');

        $residence->save();
        return redirect('residence')->with(['status' => 'Résidence enregistré avec succès.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Residence  $residence
     * @return \Illuminate\Http\Response
     */
    public function show(Residence $residence)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Residence  $residence
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $resid = Residence::find($id);
        return view('residence.edit', compact('resid'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Residence  $residence
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $resid = Residence::find($id);

        $resid->nomResidence = $request->input('nomResidence');
        $resid->ville = $request->input('ville');
        $resid->quartie = $request->input('quartie');
        $resid->type = $request->input('type');
        $resid->nbrAppEtage = $request->input('nbrEtage');
        $resid->nbrImmo = $request->input('nbrImmo');
        $resid->nbrEntre = $request->input('nbrEntre');
        $resid->espaceVert = $request->input('espaceVert');
        $resid->nbrAscenseur = $request->input('nbrAscenseur');
        $resid->nbrPark = $request->input('nbrPark');
        $resid->annee = $request->input('annee');

        $resid->save();
        return redirect('residence')->with(['status' => 'Résidence modifié avec succès.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Residence  $residence
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $resid = Residence::find($id);
        $resid -> delete();
        
        return redirect('residence');
    }

    public function getVille()
    {
        //
        $ville = Residence::select('ville')->groupBy('ville')->get();

        // Les expressions est juste.
        //$resid = DB::table('residences')->select('ville')->groupBy('ville')->get();
        
        return response()->json($ville);
    }

    public function getQuart($ville)
    {
        //
        //$quart = Residence::select('quartie')->where('ville', '=', 5)->get();
        $quart = Residence::latest()->where('ville', $ville)->get();
        // Les expressions est juste.
        //$resid = DB::table('residences')->select('ville')->groupBy('ville')->get();
        
        return response()->json($quart);
    }

    public function export()
    {
        //
        return Excel::download(new ResidenceExport, 'ListeResidences.xlsx');
    }


}
