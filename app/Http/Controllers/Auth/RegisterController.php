<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'prenom' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $result = filter_var( $data['email'], FILTER_VALIDATE_EMAIL );
        

        if($result){
            return User::create([
                'genre' => $data['genre'],
                'name' => $data['name'],
                'prenom' => $data['prenom'],
                'telephone' => $data['telephone'],
                'titreFoncier' => $data['titreFoncier'],
                'cin' => $data['cin'],
                'ville' => $data['ville'],
                'residence' => $data['residence'],
                'adresse' => $data['adresse'],
                'status' => $data['status'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);
        }else{
            return User::create([
                'genre' => $data['genre'],
                'name' => $data['name'],
                'prenom' => $data['prenom'],
                'telephone' => $data['telephone'],
                'titreFoncier' => $data['titreFoncier'],
                'cin' => $data['cin'],
                'ville' => $data['ville'],
                'residence' => $data['residence'],
                'adresse' => $data['adresse'],
                'status' => $data['status'],
                'email' => $data['email'].'@raha.ma',
                'password' => Hash::make($data['password']),
            ]);
        }

        
    }
}
