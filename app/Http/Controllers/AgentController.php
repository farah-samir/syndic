<?php

namespace App\Http\Controllers;

use App\Agent;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $agent = User::latest()->where('status', 'Agent')->paginate(5);
        return view('agent.index',compact('agent'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $agent = new User();

        $agent->genre = $request->input('genre');
        $agent->name = $request->input('name');
        $agent->prenom = $request->input('prenom');
        $agent->telephone = $request->input('telephone');
        $agent->cin = $request->input('cin');
        $agent->adresse = $request->input('adresse');
        $agent->ville = $request->input('ville');
        $agent->status = $request->input('status');
        $agent->email = $request->input('email');
        $agent->password = Hash::make($request->input('password'));
        $agent->deleted_at = $request->input('date');
        
        

        $agent->save();
        return redirect('agent')->with(['status' => 'Agent Admin enregistré avec succès.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function show(Agent $agent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $agent = User::find($id);
        return view('agent.edit', compact('agent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $agent = User::find($id);

        $agent->name = $request->input('name');
        $agent->prenom = $request->input('prenom');
        $agent->telephone = $request->input('telephone');
        $agent->cin = $request->input('cin');
        $agent->adresse = $request->input('adresse');
        $agent->email = $request->input('email');

        $agent->save();
        return redirect('agent')->with(['status' => 'Agent modifié avec succès.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        $agent = User::find($id);
        $agent->delete();

        return redirect('agent')->with(['status' => 'Agent Admin Désactivé avec succès.']);
    }

    public function trashed()
    {
        //
        $trashs = User::onlyTrashed()->where('status', 'Agent')->paginate(5);
        //dd($trashs);
        return view('agent.desactiver', compact('trashs'));
    }

    public function restorAgent(Request $request,$id)
    {
        //
        $trashs = User::withTrashed()->find($id)->restore();
        //dd($trashs);
        //return view('utilisateur.index');
        return redirect('trashed')->with(['status' => 'Agent Admin Activé avec succès.']);
        
    }

}
