<?php

namespace App\Http\Controllers;

use App\Traits\UploadTrait;
use App\Actualite;
use App\Residence;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ActualiteController extends Controller
{
    use UploadTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $actualite = Actualite::latest()->paginate(10);
        $residence = Residence::all();

        
        return view('actualite.index',compact('actualite', 'residence'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate([
            'summary-ckeditor' => 'required',
        ]);
        
        $actual = new Actualite();

        $actual->titre = $request->input('titre');
        $actual->residence_id = $request->input('residence');
        $actual->ckeditor = $request->input('summary-ckeditor');
        $actual->publier = $request->input('publier');

        $image = $request->file('image');

if(isset($image)){
            

            //$name = str_slug($request->input('name')).'_'.time();
            $name = str_slug($actual->residence_id).'_'.time();
            
            $folder = '/uploads/actualites/';
            
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            
            $this->uploadOne($image, $folder, 'public', $name);
            
            $actual->image = $filePath;
}
            
        
          


        $actual->save();
        return redirect('actualite')->with(['status' => 'Actualité enregistré avec succès.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Actualite  $actualite
     * @return \Illuminate\Http\Response
     */
    public function show(Actualite $actualite)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Actualite  $actualite
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $actualite = Actualite::find($id);

        return view('actualite.edit', compact('actualite'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Actualite  $actualite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $actual = Actualite::find($id);

        $actual->titre = $request->input('titre');
        $actual->residence_id = $request->input('residence');
        $actual->ckeditor = $request->input('summary-ckeditor');
        $actual->publier = $request->input('publier');

        $image = $request->file('image');
if(isset($image)){
            //$name = str_slug($request->input('name')).'_'.time();
            $name = str_slug($actual->residence_id).'_'.time();
            
            $folder = '/uploads/actualites/';
            
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            
            $this->uploadOne($image, $folder, 'public', $name);
            
            $actual->image = $filePath;
}
        $actual->save();
        return redirect('actualite')->with(['status' => 'Actualité modifié avec succès.']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Actualite  $actualite
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $actualite = Actualite::find($id);
        $actualite->delete();

        return redirect('actualite')->with(['status' => 'Actualite suprimer avec succée.']);
    }

    public function listActualite()
    {
        //
        $nomResid = Auth::user()->residence;

        $idResid = DB::table('residences')->select('id')->where('nomResidence', $nomResid)->pluck('id')->first();

        $actualite = Actualite::latest()->where('residence_id', $idResid)->get();

        return view('actualite.listeActualite',compact('actualite'));
    }
}
