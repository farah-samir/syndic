<?php

namespace App\Http\Controllers;

use App\Traits\UploadTrait;
use App\Reclamation;
use App\Intervention;
use App\Residence;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReclamationController extends Controller
{
    use UploadTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $reclam = Reclamation::latest()->where('visible', '1')->paginate(5);
        $intervention = Intervention::all();
        $residence = Residence::all();
        
        return view('reclamation.index',compact('reclam', 'intervention', 'residence'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $reclam = new Reclamation();

        $reclam->user_id = Auth::user()->id;
        $reclam->residence = $request->input('residence');
        $reclam->titre = $request->input('titre');
        $reclam->priorite = $request->input('priorite');
        $reclam->intervention = $request->input('intervention');
        $reclam->lieu = $request->input('lieu');
        $reclam->descript = $request->input('summary-ckeditor');
        $reclam->visible = "1";

        $image = $request->file('image');
if(isset($image)){
            //$name = str_slug($request->input('name')).'_'.time();
            $name = str_slug($reclam->user_id).'_'.time();
            
            $folder = '/uploads/reclamation/';
            
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            
            $this->uploadOne($image, $folder, 'public', $name);
            
            $reclam->image = $filePath;
}
        $reclam->save();
        return redirect('MesReclamation')->with(['status' => 'Réclamation enregistré avec succès.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reclamation  $reclamation
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('reclamation.show', ['user' => Reclamation::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reclamation  $reclamation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //->sortBy('intervention')
        //$intervention = Intervention::all();
        
        $reclam = Reclamation::find($id);
        
        $intervention = Intervention::all()->sortBy('intervention');
        
        return view('reclamation.edit', compact('reclam', 'intervention'));
    }
    
    
    public function modReclam($id)
    {
        //
        $reclam = Reclamation::find($id);
        
        $intervention = Intervention::all()->sortBy('intervention');
        
        return view('reclamation.modReclam', compact('reclam', 'intervention'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reclamation  $reclamation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $reclam = Reclamation::find($id);
        
        $reclam->residence = $request->input('residence');
        $reclam->titre = $request->input('titre');
        $reclam->priorite = $request->input('priorite');
        $reclam->intervention = $request->input('intervention');
        $reclam->lieu = $request->input('lieu');
        $reclam->descript = $request->input('descript');
        
        $reclam->save();
        return redirect('reclamation')->with(['status' => 'Réclamation modifié avec succès.']);
    }
    
    public function modReclamUpdate(Request $request, $id)
    {
        //
        $reclam = Reclamation::find($id);
        
        $reclam->residence = $request->input('residence');
        $reclam->titre = $request->input('titre');
        $reclam->priorite = $request->input('priorite');
        $reclam->intervention = $request->input('intervention');
        $reclam->lieu = $request->input('lieu');
        $reclam->descript = $request->input('descript');
        
        $reclam->save();
        return redirect('MesReclamation')->with(['status' => 'Réclamation modifié avec succès.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reclamation  $reclamation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function modReclamDestroy($id)
    {
        //
        $reclam = Reclamation::find($id);
        $reclam -> delete();
        
        return redirect('MesReclamation');
    }
    
    public function delReclamation($id)
    {
        //
        $reclam = Reclamation::find($id);
        $reclam -> delete();
        
        return redirect('reclamation');
    }
    

    public function reclam(Request $request) 
    {
        
        //
        $validatedData = $request->validate([
            'reclam' => 'required',
        ]);
        
        $delid = $request->input('reclam');

        $record = Reclamation::find($delid);
        
        return view('reclamation.fusionne', compact('record'));

    }

    public function MesReclamation(Reclamation $reclamation)
    {
        //
        $resid = Residence::select('nomResidence')->groupBy('nomResidence')->get();
        $reclam = Reclamation::latest()->where('user_id', Auth::user()->id)->paginate(5);
        //$intervention = Intervention::all();
        $intervention = Intervention::all()->sortBy('intervention');
        return view('reclamation.mesReclamation',compact('reclam', 'intervention', 'resid'));
    }

    public function fusion(Request $request) 
    {
        
        //
        $reclam = new Reclamation();

        $reclam->user_id = Auth::user()->id;
        $reclam->residence = $request->input('residence');
        $reclam->lieu = $request->input('lieu');
        $reclam->titre = $request->input('titre');
        $reclam->priorite = $request->input('priorite');
        $reclam->intervention = $request->input('intervention');
        $reclam->image = $request->input('image');
        $reclam->descript = $request->input('summary-ckeditor');
        $reclam->visible = "1";

        $reclam->save();

        $delid = $request->input('fusion');

              DB::table('reclamations')->whereIn('id', $delid)->update(array('visible' => 0));

        return redirect('reclamation')->with(['status' => 'Les réclamation sont fusionné avec succès.']);

    }

    public function recherche(Request $request)
    {
        //
        $found1 = $request->input('intervention');
        $found2 = $request->input('residence');
        
        if($found1 == 'all'){
            $reclam = Reclamation::latest()->where('residence', $found2)->where('visible', 1)->paginate(50);
        } else {
            $reclam = Reclamation::latest()->where('residence', $found2)->Where('intervention', $found1)->where('visible', 1)->paginate(50);
        }

        

        $cpt = count($reclam);

        $intervention = Intervention::all();
        $residence = Residence::all();

        
        return view('reclamation.recherche',compact('reclam', 'intervention', 'residence', 'cpt'));
    }

    public function found(Request $request)
    {
        //
        $found1 = $request->input('intervention');
        $found2 = $request->input('residence');

        //$reclam = Reclamation::latest()->where('residence', $found2)->where('intervention', $found1)->paginate(50);
        // $reclam = Reclamation::all();
        
        // return view('reclamation.recherche',compact('reclam'));
    }
    
    

}
