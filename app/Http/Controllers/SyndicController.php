<?php

namespace App\Http\Controllers;

use App\Syndic;
use App\User;
use App\Ville;
use App\Residence;
use Illuminate\Http\Request;

class SyndicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $syndic = User::latest()->where('status', 'Syndic')->whereNotNull('email_verified_at')->paginate(5);
        return view('syndic.index',compact('syndic'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Syndic  $syndic
     * @return \Illuminate\Http\Response
     */
    public function show(Syndic $syndic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Syndic  $syndic
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $syndic = User::find($id);
        
        $ville = Ville::all();
        
        $residence = Residence::all();
        
        return view('syndic.edit', compact('syndic', 'ville', 'residence'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Syndic  $syndic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $syndic = User::find($id);

        $syndic->name = $request->input('name');
        $syndic->prenom = $request->input('prenom');
        $syndic->telephone = $request->input('telephone');
        $syndic->cin = $request->input('cin');
        $syndic->adresse = $request->input('adresse');
        $syndic->email = $request->input('email');

        $syndic->save();
        return redirect('syndic')->with(['status' => 'Syndicat modifié avec succès.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Syndic  $syndic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Syndic $syndic, $id)
    {
        //
        $syndic = User::find($id);
        $syndic -> delete();
        
        return redirect('syndic');
    }

    public function inscription()
    {
        //
        $syndic = User::latest()->where('status', 'Syndic')->where('email_verified_at', null)->paginate(5);
        return view('syndic.inscription',compact('syndic'));
    }

    public function valider($id)
    {
        //
        User::where('id', $id)->update([
            'email_verified_at' => date("Y-m-d H:i:s")
            ]);
        
        return redirect('inscriptionSyndic');
    }

    public function trashed()
    {
        //
        $trashs = User::onlyTrashed()->where('status', 'Syndic')->paginate(5);
        //dd($trashs);
        return view('syndic.desactiver', compact('trashs'));
    }

    public function restor($id)
    {
        //
        $trashs = User::withTrashed()->find($id)->restore();
        //dd($trashs);
        //return view('utilisateur.index');
        return redirect('trashedSyndic');
        
    }
    
}
