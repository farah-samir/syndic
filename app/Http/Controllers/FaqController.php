<?php

namespace App\Http\Controllers;

use App\Faq;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $faq1 = Faq::latest()->where('publier', '1')->paginate(5);
        $faq0 = Faq::latest()->where('publier', '0')->paginate(5);
        
        return view('faq.index',compact('faq1', 'faq0'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $quest = new Faq();

        $quest->question = $request->input('question');
        $quest->reponse = $request->input('summary-ckeditor');
        $quest->publier = $request->input('publier');

        $quest->save();
        return redirect('faq')->with(['status' => 'La question enregistré avec succès.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function show(Faq $faq)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $faq = Faq::find($id);

        return view('faq.edit', compact('faq'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $faq = Faq::find($id);

        $faq->question = $request->input('question');
        $faq->reponse = $request->input('summary-ckeditor');
        $faq->publier = $request->input('publier');

        $faq->save();
        return redirect('faq')->with(['status' => 'F.A.Q modifié avec succès.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faq $faq)
    {
        //
    }

    public function stopper($id)
    {
        //
        DB::table('faqs')->where('id', $id)->limit(1)->update(array('publier' => 0));

        return redirect('faq')->with(['status' => 'La question est non publié.']);
    }

    public function publier($id)
    {
        //
        DB::table('faqs')->where('id', $id)->limit(1)->update(array('publier' => 1));

        return redirect('faq')->with(['status' => 'La question est publié avec succès.']);
    }

}
