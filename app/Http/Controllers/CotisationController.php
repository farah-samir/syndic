<?php

namespace App\Http\Controllers;

use App\Residence;
use App\User;

use App\Exports\ResidenceExport;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Maatwebsite\Excel\Facades\Excel;

class CotisationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $residence = Residence::all();
        
        return view('cotisation.index',compact('residence'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cotisation  $cotisation
     * @return \Illuminate\Http\Response
     */
    public function show(Cotisation  $cotisation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cotisation  $cotisation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cotisation  $cotisation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $cotisation = User::find($id);
        
        $cotisation->cotisation = $request->input('cotisation');
        
        $cotisation->save();
        
        // second part
        $found = $request->input('residence');
        
        $coprop = User::latest()->where('residence', $found)->paginate(50);
        
        $residence = Residence::all();

        return view('cotisation.index',compact('coprop', 'residence'));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cotisation  $cotisation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function recherche(Request $request)
    {
        //
        $found = $request->input('residence');
        
        //$coprop = User::latest()->where('residence', $found)->where('visible', 1)->paginate(50);
        
        $coprop = User::latest()->where('residence', $found)->paginate(50);
        
        $residence = Residence::all();

        return view('cotisation.index',compact('coprop', 'residence'));
    }
    
    public function updateCota(Request $request)
    {
        //
        $cota = $request->input('cota');
        
        $residence = $request->input('residence');
        
        $copro = User::latest()->where('residence', '=', $residence)->get();
        
        $i = 1;
        
        foreach($copro as $copros){
            
            $cotaF = $cota + $copros->cotisation;
            
            
            $affected = DB::table('users')
              ->where('id', $copros->id)
              ->update(['cotisation' => $cotaF]);
        $i+=1;  
        }
        
        return redirect('cotisation')->with(['status' => 'l\'augmentation est faite avec succès.']);
        
    }
    
    public function solde(Request $request, $id)
    {
        //
        $solde = $request->input('solde');
        
        $affected = DB::table('users')
              ->where('id', $id)
              ->update(['solde' => $solde]);
              
        // second part
        $found = $request->input('residence');
        
        $coprop = User::latest()->where('residence', $found)->paginate(50);
        
        $residence = Residence::all();

        return view('cotisation.index',compact('coprop', 'residence'));
    }


}
