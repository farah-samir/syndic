<?php

namespace App\Http\Controllers;

use App\Ville;
use App\Exports\VilleExport;
use Illuminate\Http\Request;
use Auth;

use Maatwebsite\Excel\Facades\Excel;

class VilleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    
    public function index()
    {
        //
        $ville = Ville::latest()->paginate(5);
        return view('ville.index',compact('ville'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $utilisateur = new Ville();

        $utilisateur->ville = $request->input('ville');

        $utilisateur->save();
        return redirect('ville')->with(['status' => 'Ville enregistré avec succès.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ville  $ville
     * @return \Illuminate\Http\Response
     */
    public function show(Ville $ville)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ville  $ville
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $ville = Ville::find($id);
        return view('ville.edit', compact('ville'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ville  $ville
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $ville = Ville::find($id);

        $ville->ville = $request->input('ville');

        $ville->save();
        return redirect('ville')->with(['status' => 'Ville modifié avec succès.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ville  $ville
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $ville = Ville::find($id);
        $ville -> delete();
        
        return redirect('ville');
    }

    public function export()
    {
        //
        return Excel::download(new VilleExport, 'ListeVilles.xlsx');
    }
}
