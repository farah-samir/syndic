<?php

namespace App\Http\Controllers;

use App\Traits\UploadTrait;
use App\Residence;
use App\User;
use App\Paiement;
use App\Demande;
use Auth;

use App\Exports\PaiementExport;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Maatwebsite\Excel\Facades\Excel;

class PaiementController extends Controller
{
    use UploadTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$paie = Paiement::all()->groupBy('mois','DESC')->get();
        $paie = DB::table('paiements')
                    ->select('*', DB::raw('COUNT(*) as paie'))
                    ->orderBy('mois', 'DESC')
                    ->groupBy('mois')
                    ->paginate(12);
        
                
         
        return view('paiement.index', compact('paie'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        $this->validate($request, [
            'mois' => 'required|size:4'
        ]);
       
       
        $mois = $request->input('mois');
        
        
        $syndic = User::all()->where('status', '=', 'Syndic');
        
        foreach($syndic as $syndics){
            
            for($i=01; $i<13; $i++){
            
        $Paie = new Paiement();
            
        $Paie->coprop_id = $syndics->id;
        $Paie->mois = $mois.'-'.$i;
        $Paie->balance = 0;
        $Paie->cotisation = $syndics->cotisation;
        $Paie->etat = 'Impayé';

        $Paie->save();
            }
            
        }
        
        $copro = User::all()->where('status', '=', 'Coprop');
        
        foreach($copro as $copros){
            
            for($i=01; $i<13; $i++){
                
        $Paie = new Paiement();

        $Paie->coprop_id = $copros->id;
        $Paie->mois = $mois.'-'.$i;
        $Paie->balance = 0;
        $Paie->cotisation = $copros->cotisation;
        $Paie->etat = 'Impayé';

        $Paie->save();
            }
            
        }
        
        return redirect('paiement')->with(['status' => 'Paiement du '.$mois.' est lancé avec succès.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Paiement  $paiement
     * @return \Illuminate\Http\Response
     */
    public function show(Paiement  $paiement)
    {
        //
        echo 'show page';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Paiement  $paiement
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Paiement  $paiement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //$cotisation = User::find($id);
        
        //$cotisation->cotisation = $request->input('cotisation');
        
        //$cotisation->save();
        
        // second part
        //$found = $request->input('residence');
        
        //$coprop = User::latest()->where('residence', $found)->paginate(50);
        
        //$residence = Residence::all();

        //return view('cotisation.index',compact('coprop', 'residence'));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Paiement  $paiement
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function etatPaiement()
    {
        //
        //$paie = Paiement::latest()->where('coprop_id', '=', Auth::user()->id)->get();
        
        
        $encour = Demande::orderBy('id', 'DESC')->where('user_id', Auth::user()->id)->where('statu', '0')->get();
        
        $paie = Paiement::orderBy('id', 'DESC')->where('coprop_id', Auth::user()->id)->where('balance', '0')->get();
        
        $somme = Paiement::orderBy('mois', 'DESC')->where('coprop_id', Auth::user()->id)->where('etat', 'Impayé')->sum('cotisation');
        
        
        $b1 = Paiement::where('coprop_id', Auth::user()->id)->sum('balance');
        $b2 = Paiement::where('coprop_id', Auth::user()->id)->where('etat', 'payé')->sum('cotisation');
        
        $b3 = $b1 - $b2;
                    
        return view('paiement.etatPaiement', compact('paie', 'somme', 'b3', 'encour'));
    }
    
    public function balance()
    {
        //
        $resid = User::latest()->where('status', 'Coprop')->orWhere('status', 'Syndic')->get();
        return view('paiement.balance', compact('resid'));
    }
    
    public function addBalance(Request $request)
    {
        //
        $balance = new Paiement();
        
        $balance->balance = $request->input('balance');
        $balance->coprop_id = $request->input('residt');
        
        
        $balance->save();
        
        return redirect('getbalance')->with(['status' => 'Balance est Ajouté avec succès.']);
    }
    
    public function etatPaie($id)
    {
        //
        $c_id = $id;
        $paie = Paiement::orderBy('id', 'DESC')->where('coprop_id', $id)->where('balance', '0')->get();
        
        $somme = Paiement::orderBy('mois', 'DESC')->where('coprop_id', $id)->where('etat', 'Impayé')->sum('cotisation');
        
        
        $b1 = Paiement::where('coprop_id', $id)->sum('balance');
        $b2 = Paiement::where('coprop_id', $id)->where('etat', 'payé')->sum('cotisation');
        
        $b3 = $b1 - $b2;
        
        //$soldeUser = User::where('id', $id);
        //$soldeUser = User::where('id', $id)->pluck('solde');
        //$soldeUser = User::select('solde')->where('id', $id)->get();
        $soldeUser = User::find($id)->solde;
                    
        return view('paiement.etatPaie', compact('paie', 'somme', 'b3', 'c_id', 'soldeUser'));
    }
    
    public function paieFacture(Request $request)
    {
        //
        $validatedData = $request->validate([
            'paie' => 'required',
        ]);
    
        $user_id = $request->input('user_id');
        $recu = $request->input('paie');

        $record = Paiement::find($recu);
        $tl = Paiement::find($recu)->sum('cotisation');
        
        return view('paiement.fusionne', compact('record', 'tl', 'user_id'));
    }
    
    public function demande(Request $request)
    {
        //
        $demande = new Demande();

        $demande->user_id = $request->input('user_id');
        $demande->recu = $request->input('recu');
        $demande->montant = $request->input('montant');
        $demande->mois = $request->input('mois');
        $demande->mode = $request->input('mode');
        $demande->statu = '0';
        
                $image = $request->file('image');

            $name = str_slug($demande->user_id).'_'.time();
            
            $folder = '/uploads/demandes/';
            
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            
            $this->uploadOne($image, $folder, 'public', $name);
            
            $demande->piece = $filePath;


        $demande->save();
        return redirect('etatPaiement')->with(['status' => 'Votre paiement: reçu '.$request->input('recu').' est encour.']);
        
    }
    
    public function demandePaie()
    {
        //
        $demandes = Demande::where('statu', '0')->get();
        
        return view('paiement.demandePaie', compact('demandes'));
    }
    
    public function exportPDF()
	{
	   //$data = Paiement::get()->toArray();
	   
	   //return (new InvoicesExport)->download('invoices.pdf', \Maatwebsite\Excel\Excel::DOMPDF);
	   
	   //return HTML::download(new VilleExport, 'ListeVilles.pdf');
	   //return (new VilleExport)->download('invoices.xlsx');
	   return Excel::download(new PaiementExport, 'RecuPaiement.pdf');
	   
	   /*return Excel::download('itsolutionstuff_example', function($excel) use ($data) {
		$excel->sheet('mySheet', function($sheet) use ($data)
	    {
			$sheet->fromArray($data);
	    });
	   })->download("pdf");*/
	}
	
	public function validePaie(Request $request, $id, $copro)
	{
	    //
	    //$paie_id = $request->input('id');
	    //$coprop_id = $request->input('copro');
	    
	    Paiement::where('id', $id)->update([
            'etat' => 'Payé'
            ]);
        
        return redirect('etatPaie/'.$copro);
        
	}
	
	public function validePaieGroup(Request $request)
	{
	    //
	    $c_id = $request->input('copro');
	    
	    $paie_id = $request->input('paie');
	    
	    DB::table('paiements')->whereIn('id', $paie_id)->update(array('etat' => 'Payé'));
	    
	    return redirect('etatPaie/'.$c_id);
	}
	
	public function ajoutPaie()
	{
	    //
	    echo "Ajouter paiement";
	}
	
	public function voireDemande($id)
    {
        //
        return view('paiement.voireDemande', ['dem' => Demande::findOrFail($id)]);
    }


}
