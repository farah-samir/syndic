<?php

namespace App\Http\Controllers;

use App\Partenaire;
use App\Intervention;
use App\Ville;
use Illuminate\Http\Request;

class PartenaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ville = Ville::all();
        $intervention = Intervention::all()->sortBy('intervention');
        $partenaire = Partenaire::latest()->paginate(5);
        return view('partenaire.index',compact('partenaire', 'intervention', 'ville'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $partenaire = new Partenaire();

        $partenaire->nom = $request->input('nom');
        $partenaire->domaine = $request->input('domaine');
        $partenaire->tel = $request->input('tel');
        $partenaire->email = $request->input('email');
        $partenaire->ville = $request->input('ville');

        $partenaire->save();
        return redirect('partenaire')->with(['status' => 'Partenaire enregistré avec succès.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Partenaire  $partenaire
     * @return \Illuminate\Http\Response
     */
    public function show(Partenaire $partenaire)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Partenaire  $partenaire
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $part = Partenaire::find($id);
        
        return view('partenaire.edit', compact('part'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Partenaire  $partenaire
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $part = Partenaire::find($id);

        $part->nom = $request->input('nom');
        $part->domaine = $request->input('domaine');
        $part->tel = $request->input('tel');
        $part->ville = $request->input('ville');
        $part->email = $request->input('email');

        $part->save();
        return redirect('partenaire')->with(['status' => 'Partenaire modifié avec succès.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Partenaire  $partenaire
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $part = Partenaire::find($id);
        $part -> delete();
        
        return redirect('partenaire');
    }
}
