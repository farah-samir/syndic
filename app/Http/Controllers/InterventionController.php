<?php

namespace App\Http\Controllers;

use App\Intervention;
use Illuminate\Http\Request;
use Auth;

class InterventionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }


    public function index()
    {
        //
        $domaine = Intervention::latest()->paginate(8);
        return view('intervention.index',compact('domaine'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $intervention = new Intervention();

        $intervention->intervention = $request->input('intervention');

        $intervention->save();
        return redirect('intervention')->with(['status' => 'Domaine d\'Intervention enregistré avec succès.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Intervention  $intervention
     * @return \Illuminate\Http\Response
     */
    public function show(Intervention $intervention)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Intervention  $intervention
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $interv = Intervention::find($id);
        return view('intervention.edit', compact('interv'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Intervention  $intervention
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $interv = Intervention::find($id);

        $interv->intervention = $request->input('intervention');

        $interv->save();
        return redirect('intervention')->with(['status' => 'Intervention modifié avec succès.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Intervention  $intervention
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $interv = Intervention::find($id);
        $interv -> delete();
        
        return redirect('intervention');
    }
    
    public function datatable()
    {
        return view('intervention.datatable');
    }
    
    
    
}
