<?php

namespace App\Http\Controllers;

use App\Coprop;
use App\User;
use App\Ville;
use App\Residence;
use Illuminate\Http\Request;

class CopropController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $coprop = User::latest()->where('status', 'Coprop')->whereNotNull('email_verified_at')->paginate(5);
        return view('coprop.index',compact('coprop'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coprop  $coprop
     * @return \Illuminate\Http\Response
     */
    public function show(Coprop $coprop)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coprop  $coprop
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $coprop = User::find($id);
        
        $ville = Ville::all();
        
        $residence = Residence::all();
        
        return view('coprop.edit', compact('coprop', 'ville', 'residence'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coprop  $coprop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $coprop = User::find($id);

        $coprop->name = $request->input('name');
        $coprop->prenom = $request->input('prenom');
        $coprop->telephone = $request->input('telephone');
        $coprop->cin = $request->input('cin');
        $coprop->adresse = $request->input('adresse');
        $coprop->email = $request->input('email');
        $coprop->ville = $request->input('ville');
        $coprop->residence = $request->input('residence');

        $coprop->save();
        
        return redirect('coprop')->with(['status' => 'Copropiriétaire modifié avec succès.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coprop  $coprop
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coprop $coprop, $id)
    {
        //
        $coprop = User::find($id);
        $coprop -> delete();
        
        return redirect('coprop');
    }

    public function inscription()
    {
        //
        $coprop = User::latest()->where('status', 'Coprop')->where('email_verified_at', null)->paginate(5);
        return view('coprop.inscription',compact('coprop'));
    }

    public function valider($id)
    {
        User::where('id', $id)->update([
            'email_verified_at' => date("Y-m-d H:i:s")
            ]);
        
        return redirect('inscriptionCorpro');
    }

    public function trashed()
    {
        //
        $trashs = User::onlyTrashed()->where('status', 'Coprop')->paginate(5);
        //dd($trashs);
        return view('coprop.desactiver', compact('trashs'));
    }

    public function restor($id)
    {
        //
        $trashs = User::withTrashed()->find($id)->restore();
        //dd($trashs);
        //return view('utilisateur.index');
        return redirect('trashedCoprop');
        
    }

}
