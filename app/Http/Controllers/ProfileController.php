<?php

namespace App\Http\Controllers;


use App\Traits\UploadTrait;
use App\Profile;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    use UploadTrait;

    
     /* Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $profile = User::where('id', Auth::user()->id);
        
        return view('auth.profile', compact('profile'));
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        //
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile, $id)
    {
        //
        $profile = User::find($id);
        $profile->name = $request->input('name');
        $profile->prenom = $request->input('prenom');
        $profile->adresse = $request->input('adresse');

        $image = $request->file('image');
        
        if(isset($image)){
            $name = str_slug($request->input('name')).'_'.time();
            //$name = str_slug($reclam->user_id).'_'.time();
            
            $folder = '/uploads/profile/';
            
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            
            $this->uploadOne($image, $folder, 'public', $name);
            
            $profile->image = $filePath;
        }

        $profile->save();

        return redirect('profile')->with(['status' => 'Mise à jour du profil est faite avec succès.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
    
    public function demmande(Request $request)
    {
        $demmande = new User();

        $demmande->genre = $request->input('genre');
        $demmande->name = $request->input('name');
        $demmande->prenom = $request->input('prenom');
        $demmande->telephone = $request->input('telephone');
        $demmande->titreFoncier = $request->input('titreFoncier');
        $demmande->cin = $request->input('cin');
        $demmande->ville = $request->input('ville');
        $demmande->residence = $request->input('residence');
        $demmande->adresse = $request->input('adresse');
        $demmande->status = $request->input('status');
        
        $result = filter_var( $request->input('email'), FILTER_VALIDATE_EMAIL );
        
        if($result){
            $demmande->email = $request->input('email');
        }else{
            $demmande->email = $request->input('email').'@raha.ma';
        }
        
        $demmande->password = Hash::make($request->input('password'));
        

        $demmande->save();
        return redirect('login')->with(['status' => 'Merci de votre inscription, Votre compte attente la verification de l\'administration.']);
    }
    
    
    
    
}




