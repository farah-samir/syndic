<?php

namespace App\Http\Controllers;

use App\Actualite;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $nomResid = Auth::user()->residence;

        $idResid = DB::table('residences')->select('id')->where('nomResidence', $nomResid)->pluck('id')->first();

        $actualite = Actualite::latest()->where('residence_id', $idResid)->paginate(2);
        
        $copropT = DB::table("users")->where('status', 'Coprop')->count();
        $adminT = DB::table("users")->where('status', 'Admin')->count();
        $syndicT = DB::table("users")->where('status', 'Syndic')->count();
        $agentT = DB::table("users")->where('status', 'Agent')->count();

        return view('home',compact('actualite', 'copropT', 'adminT', 'syndicT', 'agentT'));

    }
}
