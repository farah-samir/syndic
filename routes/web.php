<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Auth;
use Illuminate\Routing\Controller;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

Route::post('demmande', 'ProfileController@demmande');

Route::get('/admin', function () {

    if (Auth::check())
{
    return view('welcome');
}

    return view('auth.admin_login');
});

Route::get('/inscription', function () {
    return view('auth.inscription');
});

Route::get('pro', 'ProfileController@pro');

Route::get('profile', 'ProfileController@index');
Route::put('profile/{id}', 'ProfileController@update');

/*
|--------------------------------------------------------------------------
| Agent Admin 
|--------------------------------------------------------------------------
*/
// Route::get('agent', 'AgentController@index');
// Route::get('agent/create', 'AgentController@create');
// Route::post('agent', 'AgentController@store');
// Route::get('agent/{id}/edit', 'AgentController@edit');
// Route::put('agent/{id}', 'AgentController@update');
// Route::delete('agent/{id}', 'AgentController@destroy');

Route::resource('agent','AgentController');
Route::get('trashed', 'AgentController@trashed');
Route::get('restorAgent/{id}', 'AgentController@restorAgent');

/*
|--------------------------------------------------------------------------
| Syndic 
|--------------------------------------------------------------------------
*/
// Route::get('syndic', 'SyndicController@index');
// Route::get('syndic/create', 'SyndicController@create');
// Route::post('syndic', 'SyndicController@store');
// Route::get('syndic/{id}/edit', 'SyndicController@edit');
// Route::put('syndic/{id}', 'SyndicController@update');
// Route::delete('syndic/{id}', 'SyndicController@destroy');

Route::resource('syndic','SyndicController');
Route::get('inscriptionSyndic', 'SyndicController@inscription');
Route::get('validerSyndic/{id}', 'SyndicController@valider');
Route::delete('deleteSyndic/{id}', 'SyndicController@destroy');
Route::get('trashedSyndic', 'SyndicController@trashed');
Route::get('restorSyndic/{id}', 'SyndicController@restor');
/*
|--------------------------------------------------------------------------
| Coproprietaire 
|--------------------------------------------------------------------------
*/
// Route::get('coprop', 'CopropController@index');
// Route::get('coprop/create', 'CopropController@create');
// Route::post('coprop', 'CopropController@store');
// Route::get('coprop/{id}/edit', 'CopropController@edit');
// Route::put('coprop/{id}', 'CopropController@update');
// Route::delete('coprop/{id}', 'CopropController@destroy');

Route::resource('coprop','CopropController');
Route::get('inscriptionCorpro', 'CopropController@inscription');
Route::get('validerCorpro/{id}', 'CopropController@valider');
Route::delete('deleteCoprop/{id}', 'CopropController@destroy');
Route::get('trashedCoprop', 'CopropController@trashed');
Route::get('restorCoprop/{id}', 'CopropController@restor');

/*
|--------------------------------------------------------------------------
| Villes 
|--------------------------------------------------------------------------
*/
// Route::get('ville', 'VilleController@index');
// Route::get('ville/create', 'VilleController@create');
// Route::post('ville', 'VilleController@store');
// Route::get('ville/{id}/edit', 'VilleController@edit');
// Route::put('ville/{id}', 'VilleController@update');
// Route::delete('ville/{id}', 'VilleController@destroy');

Route::resource('ville','VilleController');
Route::get('exportVille', 'VilleController@export')->name('exportVille');
/*
|--------------------------------------------------------------------------
| Partenaires 
|--------------------------------------------------------------------------
*/
// Route::get('partenaire', 'PartenaireController@index');
// Route::get('partenaire/create', 'PartenaireController@create');
// Route::post('partenaire', 'PartenaireController@store');
// Route::get('partenaire/{id}/edit', 'PartenaireController@edit');
// Route::put('partenaire/{id}', 'PartenaireController@update');
// Route::delete('partenaire/{id}', 'PartenaireController@destroy');

Route::resource('partenaire','PartenaireController');

/*
|--------------------------------------------------------------------------
| Résidence 
|--------------------------------------------------------------------------
*/
// Route::get('residence', 'ResidenceController@index');
// Route::get('residence/create', 'ResidenceController@create');
// Route::post('residence', 'ResidenceController@store');
// Route::get('residence/{id}/edit', 'ResidenceController@edit');
// Route::put('residence/{id}', 'ResidenceController@update');
// Route::delete('residence/{id}', 'ResidenceController@destroy');

Route::resource('residence','ResidenceController');
Route::get('public/getVille','ResidenceController@getVille');
Route::get('public/getQuart/{ville}','ResidenceController@getQuart');

Route::get('exportResidence', 'ResidenceController@export')->name('exportResidence');

/*
|--------------------------------------------------------------------------
| Actualité 
|--------------------------------------------------------------------------
*/
// Route::get('actualite', 'ActualiteController@index');
// Route::get('actualite/create', 'ActualiteController@create');
// Route::post('actualite', 'ActualiteController@store');
// Route::get('actualite/{id}/edit', 'ActualiteController@edit');
// Route::put('actualite/{id}', 'ActualiteController@update');
// Route::delete('actualite/{id}', 'ActualiteController@destroy');

Route::resource('actualite','ActualiteController');
Route::get('listeActualite', 'ActualiteController@listActualite');

/*
|--------------------------------------------------------------------------
| F.A.Q 
|--------------------------------------------------------------------------
*/
// Route::get('faq', 'FaqController@index');
// Route::get('faq/create', 'FaqController@create');
// Route::post('faq', 'FaqController@store');
// Route::get('faq/{id}/edit', 'FaqController@edit');
// Route::put('faq/{id}', 'FaqController@update');
// Route::delete('faq/{id}', 'FaqController@destroy');

Route::resource('faq','FaqController');
Route::get('stopper/{id}', 'FaqController@stopper');
Route::get('publier/{id}', 'FaqController@publier');

/*
|--------------------------------------------------------------------------
| Rééclamations 
|--------------------------------------------------------------------------
*/
// Route::get('reclamation', 'ReclamationController@index');
// Route::get('reclamation/create', 'ReclamationController@create');
// Route::get('reclamation/{reclamation}', 'ReclamationController@show');
// Route::post('reclamation', 'ReclamationController@store');
// Route::get('reclamation/{id}/edit', 'ReclamationController@edit');
// Route::put('reclamation/{id}', 'ReclamationController@update');
// Route::delete('reclamation/{id}', 'ReclamationController@destroy');

Route::resource('reclamation','ReclamationController');
Route::post('reclam', 'ReclamationController@reclam');
Route::post('fusion', 'ReclamationController@fusion');
Route::get('MesReclamation', 'ReclamationController@MesReclamation');
Route::get('recherche', 'ReclamationController@recherche');
Route::post('found', 'ReclamationController@recherche');
Route::get('delReclamation/{reclamation}', 'ReclamationController@delReclamation');

Route::get('modReclam/{id}/edit', 'ReclamationController@modReclam');
Route::put('modReclam/{id}', 'ReclamationController@modReclamUpdate');
Route::get('delReclam/{id}', 'ReclamationController@modReclamDestroy');

/*
|--------------------------------------------------------------------------
| Intervention
|--------------------------------------------------------------------------
*/
// Route::get('intervention', 'InterventionController@index');
// Route::get('intervention/create', 'InterventionController@create');
// Route::post('intervention', 'InterventionController@store');
// Route::get('intervention/{id}/edit', 'InterventionController@edit');
// Route::put('intervention/{id}', 'InterventionController@update');
// Route::delete('intervention/{id}', 'InterventionController@destroy');

Route::resource('intervention','InterventionController');

/*
|--------------------------------------------------------------------------
| Cotisation
|--------------------------------------------------------------------------
*/
// Route::get('cotisation', 'CotisationController@index');
// Route::get('cotisation/create', 'CotisationController@create');
// Route::post('cotisation', 'CotisationController@store');
// Route::get('cotisation/{id}/edit', 'CotisationController@edit');
// Route::put('cotisation/{id}', 'CotisationController@update');
// Route::delete('cotisation/{id}', 'CotisationController@destroy');

Route::resource('cotisation','CotisationController');
Route::post('foundCotisation', 'CotisationController@recherche');
Route::post('updateCota', 'CotisationController@updateCota');

Route::put('solde/{id}', 'CotisationController@solde');

/*
|--------------------------------------------------------------------------
| Paiement
|--------------------------------------------------------------------------
*/
// Route::get('paiement', 'PaiementController@index');
// Route::get('paiement/create', 'PaiementController@create');
// Route::post('paiement', 'PaiementController@store');
// Route::get('paiement/{id}/edit', 'PaiementController@edit');
// Route::put('paiement/{id}', 'PaiementController@update');
// Route::delete('paiement/{id}', 'PaiementController@destroy');

Route::resource('paiement','PaiementController');
Route::get('etatPaiement', 'PaiementController@etatPaiement');
Route::get('getbalance', 'PaiementController@balance');
Route::post('addBalance', 'PaiementController@addBalance');
Route::get('etatPaie/{id}', 'PaiementController@etatPaie');
Route::post('paieFacture', 'PaiementController@paieFacture');
Route::post('demande', 'PaiementController@demande');
Route::get('demandePaie', 'PaiementController@demandePaie');

Route::get('exportPDF', 'PaiementController@exportPDF');

Route::get('validePaie/{id}/{copro}', 'PaiementController@validePaie');

Route::post('validePaieGroup', 'PaiementController@validePaieGroup');

Route::get('ajoutPaie', 'PaiementController@ajoutPaie');

Route::get('voireDemande/{id}', 'PaiementController@voireDemande');

/*
|--------------------------------------------------------------------------
| Partie des testes
|--------------------------------------------------------------------------
*/

Route::get('datatable', 'InterventionController@datatable');
