<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResidencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('residences', function (Blueprint $table) {
            $table->id();
            $table->string('nomResidence');
            $table->string('ville');
            $table->string('quartie');
            $table->string('type');
            $table->integer('nbrImmo');
            $table->integer('nbrAppEtage')->nullable();
            $table->integer('nbrEntre');
            $table->string('espaceVert');
            $table->integer('nbrAscenseur');
            $table->integer('nbrPark');
            $table->string('annee');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('residences');
    }
}
