<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('genre',10);
            $table->string('name', 20);
            $table->string('prenom', 20);
            $table->string('telephone',15);
            $table->string('titreFoncier',20)->nullable();
            $table->string('cin', 15)->nullable();
            $table->string('ville',20);
            $table->string('quartie',20)->nullable();
            $table->string('residence',50)->nullable();
            $table->string('adresse');
            $table->string('zip',10);
            $table->string('status',10);

            $table->string('email', 60)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->dateTime('deleted_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
