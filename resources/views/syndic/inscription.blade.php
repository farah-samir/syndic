@extends('layouts.master')

@section('content')



        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Syndicats</h1>
          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Liste Copropriétaires</h6>
                        </div>
                        <div class="card-body">
    
                            <nav aria-label="..." class="mt-5 float-right">
                                <ul class="pagination">
                                    {{ $syndic->links() }} 
                                </ul>
                            </nav>
    
                            <table class="table table-bordered table-responsive-lg">
                                <thead>
                                    <tr>
                                        <th scope="col"><i class="fas fa-deaf"></i></th>
                                        <th scope="col">Nom & Prénom</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Résidence</th>
                                        <th scope="col">Téléphone</th>
                                        <th scope="col">Imm Appt</th>
                                        <th scope="col">date de création</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @foreach($syndic as $syndics)
                                        <tr>
                                            <th scope="row">{{ $syndics->id }}</th>
                                            <td>{{ $syndics->name }} {{ $syndics->prenom }}</td>
                                            <td>{{ $syndics->email }}</td>
                                            <td>{{ $syndics->residence }}</td>
                                            <td>{{ $syndics->telephone }}</td>
                                            <td>{{ $syndics->adresse }}</td>
                                            <td>{{ date('d / m / Y', strtotime($syndics->created_at))}}</td>
                                            <td>
                                                <div class="dropdown no-arrow">
                                                    <a class="btn btn-outline-info btn-sm"  href="{{ url('validerSyndic/'.$syndics->id) }}" >
                                                            Valider
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
    
    
                            <nav aria-label="..." class="mt-1 float-right">
                                <ul class="pagination">
                                    {{ $syndic->links() }} 
                                </ul>
                            </nav>
    
                        </div>
                    </div>
              
                </div>
    
            </div>
   

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    
 

  @endsection

