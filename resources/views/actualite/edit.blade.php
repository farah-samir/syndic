@extends('layouts.master')

@section('content')

<style>
    .dimPib{
        width: 200px;
    }
</style>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Actualité</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">
                <i class="fas fa-address-card fa-sm text-white-50 mr-1"></i> Ajouter Actualité</a>
          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Modifier L'actualité</h6>
                        </div>
                        <div class="card-body">
    
                                <form method="POST" action="{{ url('actualite/'.$actualite->id) }}" enctype="multipart/form-data">
                                    @method('PUT')
                                    @csrf
                           
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Titre Actualité</label>
                                           <input type="text" class="form-control" name="titre" value="{{ $actualite->titre }}">
                                           </div>
                           
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Résidence</label>
                                               <select name="residence" class="form-control">
                                                    <option value="{{ $actualite->residence_id }}">{{ $actualite->residence->nomResidence }}</option> 
                                                </select>
                                           </div>
                                       </div>
                           
                                       <div class="form-row">    
                                               <div class="form-group col-lg-12">
                                                   <label class="col-form-label">Actualité</label>
                                                   <textarea class="form-control" id="summary-ckeditor" name="summary-ckeditor">{{ $actualite->ckeditor }}</textarea>
                                                   <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
                                                   <script>
                                                   CKEDITOR.replace( 'summary-ckeditor' );
                                                   </script>
                                               </div>
                                       </div> 
                           
                                       <div class="form-row">    
                                           <div class="form-group col-lg-12 ">
                                               <img src="{{ asset($actualite->image) }}" width="100px">
                                               
                                               <div>
                                                   <label for="upload">
                                                       <span id="preview"></span>
                                                   </label>
                                               </div>
                           
                                               <div class="custom-file">
                                                   <input type="file" onchange="handleFiles(files)" id="upload" name="image" multiple>
                                                   <label class="custom-file-label" for="upload">Ajouter photo</label>
                                               </div>
                                           </div>
                                       </div> 
                           
                                       <div class="form-row">    
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Publier</label>
                                               <select name="publier" class="form-control">
                                                   <option value="1">Oui</option> 
                                                   <option value="0">Non</option> 
                                               </select>
                                           </div>
                                       </div>         
                                    
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline-info btn-sm float-right">Modifier</button>
                                        </div>
                                    </form>
    
                        </div>
                    </div>
              
                </div>
    
            </div>
   

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    
 

<script>
    function handleFiles(files) {
      var imageType = /^image\//;
      for (var i = 0; i < files.length; i++) {
      var file = files[i];
      if (!imageType.test(file.type)) {
        alert("veuillez sélectionner une image");
      }else{
        if(i == 0){
          preview.innerHTML = '';
        }
        var img = document.createElement("img");
        img.classList.add("dimPib");
        img.classList.add("mt-1");
        img.classList.add("mr-2");
        img.classList.add("img-thumbnail");
        img.file = file;

        preview.appendChild(img); 
        var reader = new FileReader();
        reader.onload = ( function(aImg) { 
        return function(e) { 
        aImg.src = e.target.result; 
      }; 
     })(img);
   
    reader.readAsDataURL(file);
    }
    
    }
   }
</script>
  @endsection

