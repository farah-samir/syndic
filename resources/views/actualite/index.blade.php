@extends('layouts.master')

@section('content')



        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Actualité</h1>
            <a href="#" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">
                <i class="fas fa-address-card fa-sm text-white-50 mr-1"></i> Ajouter Actualité</a>
          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Liste Actualités</h6>
                        </div>
                        <div class="card-body">
    
                            <nav aria-label="..." class="mt-5 float-right">
                                <ul class="pagination">
                                    {{ $actualite->links() }} 
                                </ul>
                            </nav>
    
                            <table class="table table-bordered table-responsive-lg">
                                <thead>
                                    <tr>
                                        <th scope="col"><i class="fas fa-deaf"></i></th>
                                        <th scope="col">Titre</th>
                                        <th scope="col">Résidence</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @foreach($actualite as $actualites)
                                        <tr>
                                            <th scope="row">{{ $actualites->id }}</th>
                                            <td>{{ $actualites->titre }}</td>
                                            <td>{{ $actualites->residence->nomResidence }}</td>
                                            <td>{{ date('d / m / Y', strtotime($actualites->created_at))}}</td>
                                            <td>
                                                
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <form action="{{ url('actualite/'.$actualites->id) }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                        <button type="submit" class="btn btn-outline-danger btn-sm mr-1">Supprimer</button>
                                                    </form>
                                                    
                                                  <a href="{{ url('actualite/'.$actualites->id.'/edit') }}" class="btn btn-outline-info btn-sm">Modifier</a>
                                                </div>
                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
    
    
                            <nav aria-label="..." class="mt-1 float-right" >
                                <ul class="pagination">
                                    {{ $actualite->links() }} 
                                </ul>
                            </nav>
    
                        </div>
                    </div>
              
                </div>
    
            </div>
   

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    
 
 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
             <h5 class="modal-title" id="exampleModalLabel">Ajouter nouvelle Actualité</h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
             </button>
         </div>
         <div class="modal-body">
         
         <form method="POST" action="{{ url('actualite') }}" enctype="multipart/form-data">
         @csrf

            <div class="form-row">
                <div class="form-group col-lg-6">
                    <label class="col-form-label">Titre Actualité</label>
                    <input type="text" class="form-control" name="titre" required>
                </div>

                <div class="form-group col-lg-6">
                    <label class="col-form-label">Résidence</label>
                    <select name="residence" class="form-control" required>
                        <option selected disabled value="">Résidence...</option>
                        @foreach ($residence as $residences)
                            <option value="{{ $residences->id }}">{{ $residences->nomResidence }}</option> 
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-row">    
                    <div class="form-group col-lg-12">
                        <label class="col-form-label">Actualité</label>
                        <textarea class="form-control" id="summary-ckeditor" name="summary-ckeditor" required></textarea>
                        
                    </div>
            </div> 

            <div class="form-row">    
                <div class="form-group col-lg-12">
                    <div>
                        <label for="upload">
                            <span id="preview"></span>
                        </label>
                    </div>

                    <div class="custom-file">
                        <input type="file" onchange="handleFiles(files)" id="upload" name="image" multiple required>
                        <label class="custom-file-label" for="upload">Ajouter photo</label>
                    </div>
                </div>
            </div> 

            <div class="form-row">    
                <div class="form-group col-lg-6">
                    <label class="col-form-label">Publier</label>
                    <select name="publier" class="form-control">
                        <option value="1">Oui</option> 
                        <option value="0">Non</option> 
                    </select>
                </div>
            </div>         
         
             <div class="modal-footer">
                 <button type="button" class="btn btn-outline-dark btn-sm float-right" data-dismiss="modal">Fermer</button>
                 <button type="submit" class="btn btn-outline-info btn-sm float-right">Ajouter</button>
             </div>
         </form>
         
         </div>
 
      </div>
    </div>
</div>

<script>
    function handleFiles(files) {
      var imageType = /^image\//;
      for (var i = 0; i < files.length; i++) {
      var file = files[i];
      if (!imageType.test(file.type)) {
        alert("veuillez sélectionner une image");
      }else{
        if(i == 0){
          preview.innerHTML = '';
        }
        var img = document.createElement("img");
        img.classList.add("dimPib");
        img.classList.add("mt-1");
        img.classList.add("mr-2");
        img.classList.add("img-thumbnail");
        img.file = file;

        preview.appendChild(img); 
        var reader = new FileReader();
        reader.onload = ( function(aImg) { 
        return function(e) { 
        aImg.src = e.target.result; 
      }; 
     })(img);
   
    reader.readAsDataURL(file);
    }
    
    }
   }
</script>
  @endsection

