@extends('layouts.master')

@section('content')



        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Actualités</h1>
          </div>
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-body">
    
                                <div class="accordion" id="accordionExample">

                                        @foreach ($actualite as $actualites)
                                            
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <h2 class="mb-0">
                                                  <button class="btn btn-link text-left" type="button" data-toggle="collapse" data-target="#collapse{{ $actualites->id }}" 
                                                                                                    aria-expanded="true" aria-controls="collapse{{ $actualites->id }}">
                                                    {{ $actualites->titre }} 
                                                  </button> 
                                                <span class="float-right" style="font-size:12px;">
                                                        Date publication : {{ date('d / m / Y', strtotime($actualites->created_at)) }} 
                                                </span>
        
                                                </h2>
                                            </div>
                                          
                                            <div id="collapse{{ $actualites->id }}" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            @if(isset($actualites->image))
                                                                <img class="img-fluid" src="{{ url($actualites->image) }}"  /> 
                                                            @else
                                                                <img class="img-fluid" src="{{ asset('assets/img/no-image.png') }}" /> 
                                                            @endif
                                                            
                                                        </div>
                                                        <div class="col-md-8">
                                                                {!! $actualites->ckeditor !!}
                                                        </div>
                                                    </div>
                                                        
                                                </div>
                                            </div>
                                        </div>
        
                                        @endforeach
        
                                    </div>

                        </div>
                    </div>
              
                </div>
    
            </div>
   

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    

  @endsection

