@extends('layouts.master')

@section('content')



        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Réclamations</h1>

          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->

          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                    <form action="{{ url('reclam') }}" method="post">
                        @csrf
    
                        <div class="card-header py-3">
                            
                            @if (Auth::user()->status == "Admin" OR Auth::user()->status == "Agent")
                                <button class="d-sm-inline-block btn btn-sm btn-primary shadow-sm float-right mr-2" type="submit">
                                    <i class="fas fa-address-card fa-sm text-white-50 mr-1"></i> Fusionner</button>
                            @endif
    
                            <h6 class="m-0 font-weight-bold text-primary">Liste Réclamations *=*</h6>
                        </div>
                        <div class="card-body">
    
                            <nav aria-label="..." class="mt-5 float-right">
                                <ul class="pagination">
                                    {{ $reclam->links() }} 
                                </ul>
                            </nav>
    
                            <table class="table table-bordered table-responsive-lg">
                                <thead>
                                    <tr>
                                        <th scope="col"><i class="fas fa-deaf"></i></th>
                                        <th scope="col">N.Réc</th>
                                        <th scope="col">Titre</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @foreach($reclam as $reclams)
                                        <tr>
                                            <th scope="row">
                                                <div class="form-group form-check">
                                                    <input type="checkbox" class="form-check-input" 
                                                        value="{{ $reclams->id }}" name="reclam[]" >
                                                </div>
                                            </th>
                                            <td>{{ $reclams->id }}</td>
                                            <td>{{ $reclams->titre }}</td>
                                            <td>
                                                
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                  <a href="{{ url('reclamation/'.$reclams->id) }}" class="btn btn-outline-success btn-sm">Voir</a>
                                                  <a href="{{ url('reclamation/'.$reclams->id.'/edit') }}" class="btn btn-outline-info btn-sm">Modifier</a>
                                                  <a href="{{ url('delReclamation/'.$reclams->id) }}" class="btn btn-outline-danger btn-sm">Supprimer</a>
                                                </div>

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
    
    
                            <nav aria-label="..." class="mt-1 float-right">
                                <ul class="pagination">
                                    {{ $reclam->links() }} 
                                </ul>
                            </nav>
    
                        </div>
                    </form>
                    </div>
              
                </div>
    
            </div>
    
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    
 
 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
             <h5 class="modal-title" id="exampleModalLabel">Ajouter réclamation</h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
             </button>
         </div>
         <div class="modal-body">
         
                <form action="{{ url('reclamation') }}" method="post">
                        @csrf

                        <div class="form-row">
                                <div class="form-group col-lg-8">
                                    <label for="exampleInputEmail1">Titre Réclamation</label>
                                    <input type="text" class="form-control" name="titre">
                                </div>

                                <div class="form-group col-lg-4">
                                    <label for="exampleFormControlSelect1">Priorité</label>
                                    <select class="form-control" name="priorite">
                                        <option>Urgent</option>
                                        <option>Fréquent</option>
                                    </select>
                                </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlSelect2">Domaine d'intervention</label>
                            <select multiple class="form-control" name="intervention">
                              @foreach($intervention as $interventions)
                                <option>{{ $interventions->intervention }}</option>
                              @endforeach
                            </select>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-8">
                                <label for="exampleInputEmail1">Lieu</label>
                                <input type="text" class="form-control" name="lieu">
                            </div>
                        </div> 

                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Contenu de la réclamation</label>
                            <textarea class="form-control" id="summary-ckeditor" name="summary-ckeditor"></textarea>
                            <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
                            <script>
                            CKEDITOR.replace( 'summary-ckeditor' );
                            </script>
                        </div>

                        <button type="submit" class="btn btn-primary">Créer Réclamation</button>
                    </form>
         
         </div>
 
      </div>
    </div>
</div>

  @endsection

