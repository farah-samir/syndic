@extends('layouts.master')

@section('content')

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Réclamations </h1>
          </div>
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Réclamation</h6>
                        </div>
                        <div class="card-body">
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                               <label class="col-form-label">Titre : <strong>{{ $user->titre }}</strong></label>
                                            </div>
                                        </div> 
                                        
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                               <label class="col-form-label">Résidence : <strong>{{ $user->residence }}</strong></label>
                                            </div>
                                        </div> 
                                        
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                               <label class="col-form-label">Intervention : <strong>{{ $user->intervention }}</strong></label>
                                            </div>
                                        </div> 
                                        
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                               <label class="col-form-label">Lieu : <strong>{{ $user->lieu }}</strong></label>
                                            </div>
                                        </div> 
                                        
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                               <label class="col-form-label">Priorite : <strong>{{ $user->priorite }}</strong></label>
                                            </div>
                                        </div> 
                                        
                                        <div class="form-group">
                                            <label for="exampleFormControlTextarea1">Contenu de la réclamation : <strong>{!! $user->descript !!}</strong></label>
                                        </div>
                                        
                                        @if(isset($user->image))
                                        <div class="form-group">
                                            <label for="exampleFormControlTextarea1">Image</label>
                                            <img src="{{ url($user->image) }}" class="img-fluid" width="200px">
                                        </div>
                                        @endif
    
                        </div>
                    </div>
              
                </div>
    
            </div>
   

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    


  @endsection

