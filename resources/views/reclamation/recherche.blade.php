@extends('layouts.master')

@section('content')



        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Recherche réclamation</h1>
          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Recherche-</h6>
                        </div>
                        <div class="card-body">
    
                                <form class="needs-validation" action="{{ url('found') }}" method="post">
                                    @csrf
                                    <div class="form-row">
        
                                        <div class="col-md-3 mb-3">
                                            <select class="form-control myselect" name="residence" required>
                                                <option disabled selected value="">Résidence</option>
                                                @foreach($residence as $residences) 
                                                    <option value="{{ $residences->nomResidence}}">{{ $residences->nomResidence}}</option>
                                                @endforeach
                                            </select>
        
                                            <script type="text/javascript">
                                                $(".myselect").select2();
                                            </script> 
        
                                            <div class="valid-tooltip">
                                                Looks good!
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3 mb-3">
                                            <select class="form-control myselectTwo" name="intervention" required>
                                                <option disabled selected value="">Domaine intervention</option>
                                                    <option value="all">Tout</option>
                                                @foreach($intervention as $interventions) 
                                                    <option value="{{ $interventions->intervention}}">{{ $interventions->intervention}}</option>
                                                @endforeach
                                            </select>
            
                                            <script type="text/javascript">
                                                $(".myselectTwo").select2();
                                            </script> 
            
                                            <div class="valid-tooltip">
                                                Looks good!
                                            </div>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <button class="btn-shadow btn btn-info btn-sm" type="submit">Recherche</button>
                                        </div>
                                    </div>
                                </form>
    
                        </div>
                    </div>
              
                </div>
    
            </div>

            <div class="row">
                <div class="col-md-12">

                    @if($cpt == 0)
                        <p>Rien a afficher</p>
                    @else
                        
                        <div class="row">
                            <div class="col-lg-12">
                          
                                <div class="card shadow mb-4">
                                <form action="{{ url('reclam') }}" method="post">
                                    @csrf
                
                                    <div class="card-header py-3">
                                        

                                        <h6 class="m-0 font-weight-bold text-primary">Liste Réclamations</h6>
                                    </div>
                                    <div class="card-body">
                
                                        <nav aria-label="..." class="mt-5 float-right">
                                            <ul class="pagination">
                                                {{ $reclam->links() }} 
                                            </ul>
                                        </nav>
                
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th scope="col">N.Réc</th>
                                                    <th scope="col">Titre</th>
                                                    <th scope="col">Priorité</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Intervetion</th>
                                                    <th scope="col">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                @foreach($reclam as $reclams)
                                                    <tr>
                                                        <td>{{ $reclams->id }}</td>
                                                        <td>{{ $reclams->titre }}</td>
                                                        <td>{{ $reclams->priorite }}</td>
                                                        <td>{{ date('d / m / Y', strtotime($reclams->created_at))}}</td>
                                                        <td>{!! $reclams->intervention !!}</td>
                                                        <td>
                                                            <div class="dropdown no-arrow">
        
                                                                <a class="btn btn-outline-danger btn-sm float-right mr-2" 
                                                                href="{{ url('delReclamation/'.$reclams->id) }}">Supprimer</a>
                                                                
                                                                <a class="btn btn-outline-info btn-sm float-right mr-2" 
                                                                href="{{ url('reclamation/'.$reclams->id.'/edit') }}">Modifier</a>
                                                                
                                                                <a class="btn btn-outline-success btn-sm float-right mr-2" 
                                                                href="{{ url('reclamation/'.$reclams->id) }}">Voire</a>
                                                                
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                
                                        <nav aria-label="..." class="mt-1 float-right">
                                            <ul class="pagination">
                                                {{ $reclam->links() }} 
                                            </ul>
                                        </nav>
                
                                    </div>
                                </form>
                                </div>
                          
                            </div>
                
                        </div>

                    @endif





                </div>
            </div>
   
            <div class="row">
                
            </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    
 


  @endsection

