@extends('layouts.master')

@section('content')



        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Fusionner les Réclamations</h1>

          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                     
    
                        <div class="card-header py-3">
    
                            <h6 class="m-0 font-weight-bold text-primary">Fusionne Réclamations</h6>
                        </div>
                        <div class="card-body">
    
                                
    
                           
                                <div class="row">
                                        <div class="col-md-12">
                                        <form action="{{ url('fusion') }}" method="post">
                                        @csrf
                                        
                                        
                                        
                                        @foreach ($record as $records) 
                                        
                                        
                                        <div class="form-row mb-5">
                                            @if(isset($records->image))
                                                <div class="col">
                                                    <div class="form-check">
                                                        <label for="exampleFormControlTextarea1">Image</label>
                                                        <img src="{{ url($records->image) }}" width="200px"><br>
                                                      <input class="form-check-input" type="radio" name="image" value="{{ $records->image }}" checked>
                                                    </div> 
                                                </div>
                                            @endif
                                        </div>
                                        
                                        
                                        @endforeach
                                        
                                        

                                        <div class="form-group">
                                                <label for="exampleFormControlSelect1">Résidence</label>
                                                <select class="form-control" name="residence">
                                                    @foreach ($record as $records)   
                                                    <option value="{{ $records->residence }}">{{ $records->residence }}</option>
                                                    @endforeach
                                                </select>
                                        </div>

                                        <div class="form-group">
                                                <label for="exampleFormControlSelect1">Lieu</label>
                                                <select class="form-control" name="lieu">
                                                    @foreach ($record as $records)   
                                                    <option value="{{ $records->lieu }}">{{ $records->lieu }}</option>
                                                    @endforeach
                                                </select>
                                        </div>
            
                                            <label for="basic-url">Les titres a fusionner</label>
                                        @foreach ($record as $records)
                                            {{-- <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon3">Titre de : {{ $records->user->name }}</span>
                                                </div>
                                                <input type="text" class="form-control" value="{{ $records->titre }}">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                            </div>
                                                        </span>
                                                    </div>
                                            </div> --}}
                                            <input type="hidden" value="{{ $records->id }}" name="fusion[]" placeholder="{{ $records->id }}">
                                        @endforeach
                                            
                                            <br><br>
            
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Titre</label>
                                                <select class="form-control" name="titre">
                                                    @foreach ($record as $records)   
                                                    <option>{{ $records->titre }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
            
                                            <br><br>
            
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">interven</label>
                                                <select class="form-control" name="intervention">
                                                    @foreach ($record as $records)   
                                                    <option>{{ $records->intervention }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
            
                                            <br><br>
            
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Priorité</label>
                                                <select class="form-control" name="priorite">
                                                    <option>Urgent</option>
                                                    <option>Moyenne</option>
                                                    <option>Normal</option>
                                                </select>
                                            </div>
                
                                                <br><br>
                                            <div class="form-group">
                                                <label for="exampleFormControlTextarea1">Message de la réclamation</label>
                                                <textarea class="form-control" id="summary-ckeditor" name="summary-ckeditor">
                                                            @foreach ($record as $records)
                                                            <strong><em>{{ $records->user->name }} :</em></strong> {{ $records->descript }}
                                                            @endforeach
                                                </textarea>
                                                    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
                                                    <script>
                                                    CKEDITOR.replace( 'summary-ckeditor' );
                                                    </script>
                                            </div>
            
                                                <button type="submit" class="btn btn-primary">Fusionner</button>
                                            </form>
                                        </div>
                                    </div>
    
                            
    
                        </div>
                     
                    </div>
              
                </div>
    
            </div>
    
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    
 

  @endsection

