@extends('layouts.master')

@section('content')



        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Réclamations</h1>
            <a href="#" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm " 
                                                data-toggle="modal" 
                                                data-target="#exampleModal" 
                                                data-whatever="@getbootstrap">
                <i class="fas fa-address-card fa-sm text-white-50 mr-1"></i> Ajouter Réclamation</a>
          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Liste Réclamations</h6>
                        </div>
                        <div class="card-body">
    
                                <nav aria-label="..." class="mt-5 float-right">
                                        <ul class="pagination">
                                            {{ $reclam->links() }} 
                                        </ul>
                                    </nav>
            
                                    <table class="table table-bordered table-responsive-lg">
                                        <thead>
                                            <tr>
                                                
                                                <th scope="col">№</th>
                                                <th scope="col">Titre</th>
                                                <th scope="col">Etat</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            @foreach($reclam as $reclams)
                                                <tr>
                                                    <td>{{ $reclams->id }}</td>
                                                    <td>{{ $reclams->titre }}</td>
                                                    <td>
                                                        
                                                        <div >
                                                            
                                                            <a href="{{ url('delReclam/'.$reclams->id) }}" class="btn btn-outline-danger btn-sm">
                                                                <i class="fas fa-trash-alt"></i>
                                                            </a>
                                                            <br>
                                                            <a href="{{ url('modReclam/'.$reclams->id.'/edit') }}" class="btn btn-outline-info btn-sm mt-1">
                                                                <i class="fas fa-edit"></i>
                                                            </a>
                                                        </div>
                                                
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
            
            
                                    <nav aria-label="..." class="mt-1 float-right">
                                        <ul class="pagination">
                                            {{ $reclam->links() }} 
                                        </ul>
                                    </nav>
    
                        </div>
                    </div>
              
                </div>
    
            </div>
   

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    
 
 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel">Ajouter réclamation Résidence : {{ Auth::user()->residence }}</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
    
             
             <div class="modal-body">
             
                    <form action="{{ url('reclamation') }}" method="post" enctype="multipart/form-data">
                            @csrf
    
                            @if( Auth::user()->status == 'Syndic' OR Auth::user()->status == 'Coprop' )
                                <div class="form-row">
                                    <div class="form-group col-lg-8">
                                        <input type="hidden" class="form-control"  value="{{ Auth::user()->residence }}" name="residence" >
                                    </div>
                                </div> 
                            @endif
    
                            @if( Auth::user()->status == 'Admin' OR Auth::user()->status == 'Agent')
                                <div class="form-row">
                                    <div class="form-group col-lg-12">
                                        <label for="exampleInputEmail1">Résidence</label>
                                        <select class="form-control" name="residence">
                                            @foreach($resid as $resids)
                                              <option value="{{ $resids->nomResidence }}">{{ $resids->nomResidence }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
    
                            <div class="form-row">
                                    <div class="form-group col-lg-12">
                                        <label for="exampleInputEmail1">Titre Réclamation</label>
                                        <input type="text" class="form-control" name="titre" maxlength = "30" required>
                                    </div>
                            </div> 
    
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="priorite" id="prio1" value="Urgent" checked>
                                <label class="form-check-label" for="prio1">
                                    Urgent
                                </label>
                            </div>
    
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="priorite" id="prio2" value="Fréquent" >
                                <label class="form-check-label" for="prio2">
                                    Fréquent
                                </label>
                            </div>
    
                            <br>
                            <div class="form-group">
                                <label for="exampleFormControlSelect2">Domaine d'intervention</label>
                                <select  class="form-control" name="intervention">
                                  @foreach($intervention as $interventions)
                                    <option>{{ $interventions->intervention }}</option>
                                  @endforeach
                                </select>
                            </div>
    
                            <div class="form-row">
                                <div class="form-group col-lg-12">
                                    <label for="exampleInputEmail1">Lieu</label>
                                    <input type="text" class="form-control" name="lieu" required>
                                </div>
                            </div> 
    
                            <div class="form-row">    
                                <div class="form-group col-lg-12">
                                    <div>
                                        <label for="upload">
                                            <span id="preview"></span>
                                        </label>
                                    </div>
                
                                    <div class="custom-file">
                                        <input type="file" onchange="handleFiles(files)" id="upload" name="image" >
                                        <label class="custom-file-label" for="upload">Ajouter Photo</label>
                                    </div>
                                </div>
                            </div> 
    
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Contenu de la réclamation</label>
                                <textarea class="form-control" id="summary-ckeditor" name="summary-ckeditor" required></textarea>
                            </div>
    
                            <button type="submit" class="btn btn-primary">Créer Réclamation</button>
                        </form>
             
             </div>
     
          </div>
        </div>
    </div>
    
    <script>
        function handleFiles(files) {
          var imageType = /^image\//;
          for (var i = 0; i < files.length; i++) {
          var file = files[i];
          if (!imageType.test(file.type)) {
            alert("veuillez sélectionner une image");
          }else{
            if(i == 0){
              preview.innerHTML = '';
            }
            var img = document.createElement("img");
            img.classList.add("dimPib");
            img.classList.add("mt-1");
            img.classList.add("mr-2");
            img.classList.add("img-thumbnail");
            img.file = file;
    
            preview.appendChild(img); 
            var reader = new FileReader();
            reader.onload = ( function(aImg) { 
            return function(e) { 
            aImg.src = e.target.result; 
          }; 
         })(img);
       
        reader.readAsDataURL(file);
        }
        
        }
       }
    </script>

  @endsection

