@extends('layouts.master')

@section('content')

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Réclamations </h1>
          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Modifier réclamation</h6>
                        </div>
                        <div class="card-body">
    
                                <form method="POST" action="{{ url('modReclam/'.$reclam->id) }}">
                                    @method('PUT')
                                    @csrf
                           
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                               <label class="col-form-label">Titre</label>
                                            <input type="text" class="form-control" name="titre" value="{{ $reclam->titre }}">
                                            </div>
                                        </div> 
                                        
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                               <label class="col-form-label">Résidence</label>
                                               <input type="hidden" class="form-control" name="residence" value="{{ $reclam->residence }}" >
                                            <input type="text" class="form-control" value="{{ $reclam->residence }}" disabled>
                                            </div>
                                        </div> 
                                        
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                                <label for="exampleFormControlSelect2">Domaine d'intervention</label>
                                                <select  class="form-control" name="intervention">
                                                    
                                                    <option>{{ $reclam->intervention }}</option>
                                                @foreach($intervention as $inv)
                                                    <option>{{ $inv->intervention }}</option>
                                                @endforeach
                                                 
                                                </select>
                                            </div>
                                        </div> 
                                        
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                               <label class="col-form-label">Lieu</label>
                                            <input type="text" class="form-control" name="lieu" value="{{ $reclam->lieu }}">
                                            </div>
                                        </div> 
                                        
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                               <label class="col-form-label">Priorite</label>
                                            <input type="text" class="form-control" name="priorite" value="{{ $reclam->priorite }}">
                                            </div>
                                        </div> 
                                        
                                        <div class="form-group">
                                            <label for="exampleFormControlTextarea1">Contenu de la réclamation</label>
                                            <textarea class="form-control" name="descript">{{ $reclam->descript }}</textarea>
                                        </div>
                                    
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline-info btn-sm float-right">Modifier</button>
                                        </div>
                                    </form>
    
                        </div>
                    </div>
              
                </div>
    
            </div>
   

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    


  @endsection

