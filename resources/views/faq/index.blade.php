@extends('layouts.master')

@section('content')



        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Foire aux questions</h1>
            @if(Auth::user()->status == "Admin" or Auth::user()->status == "Agent")
            <a href="#" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">
                <i class="fas fa-address-card fa-sm text-white-50 mr-1"></i> Ajouter question</a>
            @endif   
          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
            @if(Auth::user()->status == "Admin" or Auth::user()->status == "Agent")
            <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Questions non publié</h6>
                        </div>
                        <div class="card-body">
    
                                <div class="accordion" id="accordionExample">

                                        @foreach ($faq0 as $faq0s)
                                            
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <h2 class="mb-0">
                                                  <button class="btn btn-link text-left" type="button" data-toggle="collapse" data-target="#collapse{{ $faq0s->id }}" 
                                                                                                    aria-expanded="true" aria-controls="collapse{{ $faq0s->id }}">
                                                    {{ $faq0s->question }}
                                                  </button>
                                                  
                                                  
                                                  <a href="{{ url('publier/'.$faq0s->id) }}" class="btn btn-outline-danger btn-sm float-right">Publier</a>
                                                <a class="btn btn-outline-info btn-sm float-right mr-2" href="{{ url('faq/'.$faq0s->id.'/edit') }}">Modifier</a>
                                                 
        
                                                </h2>
                                            </div>
                                          
                                            <div id="collapse{{ $faq0s->id }}" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                                                <div class="card-body">
                                                        {!! $faq0s->reponse !!}
                                                </div>
                                            </div>
                                        </div>
        
                                        @endforeach
        
                                    </div>

                        </div>
                    </div>
              
                </div>
    
            </div>
            @endif
            <div class="row">

                    <div class="col-lg-12">
                  
                        <div class="card shadow mb-4">
                            <div class="card-body">
        
                                    <div class="accordion" id="accordionExample02">
    
                                            @foreach ($faq1 as $faq1s)
                                                
                                            <div class="card">
                                                <div class="card-header" id="headingOne">
                                                    <h2 class="mb-0">
                                                      <button class="btn btn-link text-left" type="button" data-toggle="collapse" data-target="#collapse{{ $faq1s->id }}" 
                                                                                                        aria-expanded="true" aria-controls="collapse{{ $faq1s->id }}">
                                                        {{ $faq1s->question }}
                                                      </button>
                                                      
                                                    @if(Auth::user()->status == "Admin" or Auth::user()->status == "Agent")
                                                    <a href="{{ url('stopper/'.$faq1s->id) }}" class="btn btn-outline-success btn-sm float-right"> Arrêter</a>
                                                    <a class="btn btn-outline-info btn-sm float-right mr-2" href="{{ url('faq/'.$faq1s->id.'/edit') }}">Modifier</a>
                                                    @endif
            
                                                    </h2>
                                                </div>
                                              
                                                <div id="collapse{{ $faq1s->id }}" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample02">
                                                    <div class="card-body">
                                                            {!! $faq1s->reponse !!}
                                                    </div>
                                                </div>
                                            </div>
            
                                            @endforeach
            
                                        </div>
    
                            </div>
                        </div>
                  
                    </div>
        
            </div>
   

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    
 
 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
             <h5 class="modal-title" id="exampleModalLabel">Ajouter nouvelle Question</h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
             </button>
         </div>
         <div class="modal-body">
         
                <form action="{{ url('faq') }}" method="post">
                        @csrf
                        <div class="form-group">
                        <label for="exampleInputEmail1">Titre Questions</label>
                        <input type="text" class="form-control" name="question" required>
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Reponse</label>
                            <textarea class="form-control" id="summary-ckeditor" name="summary-ckeditor"></textarea>
                            <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
                            <script>
                            CKEDITOR.replace( 'summary-ckeditor' );
                            </script>
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Publier</label>
                            <select class="form-control" name="publier">
                            <option value="1">Oui</option>
                            <option value="0">Non</option>
                            </select>
                        </div>

                        <button type="submit" class="btn btn-primary">Créer</button>
                    </form>
         
         </div>
 
      </div>
    </div>
</div>
  @endsection

