@extends('layouts.master')

@section('content')

<style>
    .dimPib{
        width: 200px;
    }
</style>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Foire aux questions </h1>
          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Modifier faq</h6>
                        </div>
                        <div class="card-body">
    
                                <form method="POST" action="{{ url('faq/'.$faq->id) }}">
                                    @method('PUT')
                                    @csrf
                           
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Titre Actualité</label>
                                           <input type="text" class="form-control" name="question" value="{{ $faq->question }}">
                                           </div>
                                       </div>
                           
                                       <div class="form-row">    
                                               <div class="form-group col-lg-12">
                                                   <label class="col-form-label">Actualité</label>
                                                   <textarea class="form-control" id="summary-ckeditor" name="summary-ckeditor">{{ $faq->reponse }}</textarea>
                                                   <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
                                                   <script>
                                                   CKEDITOR.replace( 'summary-ckeditor' );
                                                   </script>
                                               </div>
                                       </div> 
                           
                                       <div class="form-row">    
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Publier</label>
                                               <select name="publier" class="form-control">
                                                   <option value="1">Oui</option> 
                                                   <option value="0">Non</option> 
                                               </select>
                                           </div>
                                       </div>         
                                    
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary">Modifier</button>
                                        </div>
                                    </form>
    
                        </div>
                    </div>
              
                </div>
    
            </div>
   

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    


  @endsection

