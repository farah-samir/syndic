<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

        <!-- Sidebar Toggle (Topbar) -->
        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
          <i class="fa fa-bars"></i>
        </button>

        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">

          <!-- Nav Item - Search Dropdown (Visible Only XS) -->
          <li class="nav-item dropdown no-arrow d-sm-none">
            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-search fa-fw"></i>
            </a>
            <!-- Dropdown - Messages -->
            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
              <form class="form-inline mr-auto w-100 navbar-search">
                <div class="input-group">
                  <input type="text" class="form-control bg-light border-0 small" placeholder="Recherche..." aria-label="Search" aria-describedby="basic-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-primary" type="button">
                      <i class="fas fa-search fa-sm"></i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </li>

          

          <!-- Nav Item - Messages 
          <li class="nav-item dropdown no-arrow mx-1">
            <a class="nav-link dropdown-toggle"  onclick="show_hideMess()">
              <i class="fas fa-envelope fa-fw"></i>
              
              <span class="badge badge-danger badge-counter">7</span>
            </a>
            
            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" id="messagesDropdown" style="display:none;">
              <h6 class="dropdown-header">
                Message Center
              </h6>

              <a class="dropdown-item d-flex align-items-center" href="#">
                <div class="dropdown-list-image mr-3">
                  <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
                  <div class="status-indicator bg-success"></div>
                </div>
                <div class="font-weight-bold">
                  <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>
                  <div class="small text-gray-500">Emily Fowler · 58m</div>
                </div>
              </a>
              
              <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
            </div>
          </li>-->
          
            @if(Auth::user()->status != "Admin" and Auth::user()->status != "Agent")
            
                <li class="nav-item dropdown no-arrow">
                    <a class="nav-link dropdown-toggle">
                        <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                            Prix de cotisation : <span class="text-danger"><strong>{{ Auth::user()->cotisation }} MAD</strong></span>
                        </span>
                    </a>
                </li>
                
                <div class="topbar-divider d-none d-sm-block"></div>
                
                <li class="nav-item dropdown no-arrow">
                    <a class="nav-link dropdown-toggle">
                        <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                            Solde initial : <span class="text-danger"><strong>{{ Auth::user()->solde }} MAD</strong></span>
                        </span>
                    </a>
                </li>
                
            @endif

          <div class="topbar-divider d-none d-sm-block"></div>
    
          <!-- Nav Item - User Information -->
          <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" onclick="show_hide()" >
              <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ Auth::user()->name }} {{ Auth::user()->prenom }}</span>
              
              
              
                @if(isset(Auth::user()->image))
                    <img src="{{ asset(Auth::user()->image) }}" class="img-profile rounded-circle border border-primary"> 
                @else
                    <img class="img-profile rounded-circle border border-primary" src="{{ asset('assets/img/profil.png') }}"> 
                @endif
                                                        
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" style="display:none;"  id="dropContent">
              <a class="dropdown-item" href="{{ url('profile') }}">
                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                Profil
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="{{ url('logout') }}" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Se déconnecter
                </a>
            </div>
          </li>

        </ul>
<script>
function show_hide(){

  var click = document.getElementById("dropContent");
  if (click.style.display === "none"){
    click.style.display = "block";
  }else{
    click.style.display = "none";
  }
  
}
function show_hideMess(){

  var click = document.getElementById("messagesDropdown");
  if (click.style.display === "none"){
    click.style.display = "block";
  }else{
    click.style.display = "none";
  }

}
</script>
      </nav>