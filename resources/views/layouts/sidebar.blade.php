<ul class="navbar-nav {{ (Auth::user()->status == "Admin") ? 'bg-gradient-primary' : 'bg-Syndic' }} sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('home') }}">
            <div class="sidebar-brand-icon rotate-n-90">
                <!-- <i class="fas fa-building"></i> -->
                <img src="{{ asset('assets/img/logo.png') }}" width="50px">
            </div>
          <!-- <div class="sidebar-brand-text mx-1">Raha</div> -->
        </a>
  
        <!-- Divider -->
        <hr class="sidebar-divider my-0">
  
        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
          <a class="nav-link" href="{{ url('home') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>ACCUEIL </span></a>
        </li>
  
        <!-- Divider -->
        <hr class="sidebar-divider">
  
        <!-- Heading -->
        @if(Auth::user()->status == "Admin")
        <div class="sidebar-heading">
          Gestion Des Comptes
        </div>
  
        <!-- Agent Admin -->
        
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <i class="fas fa-fw fa-cog"></i>
            <span>Agent admin</span>
          </a>
          <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <h6 class="collapse-header">Opérations:</h6>
              <a class="collapse-item {{ (request()->is('agent')) ? 'active' : '' }}" href="{{ url('agent') }}">Comptes agent admin</a>
              <a class="collapse-item {{ (request()->is('trashed')) ? 'active' : '' }}" href="{{ url('trashed') }}">Comptes Désactivé</a>
            </div>
          </div>
        </li>
        @endif

        <!-- Syndicat -->
        @if(Auth::user()->status != "Syndic")
        @if(Auth::user()->status != "Coprop")
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
              <span>Syndicat</span>
          </a>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <h6 class="collapse-header">Opérations:</h6>
            <a class="collapse-item {{ (request()->is('syndic')) ? 'active' : '' }}" href="{{ url('syndic') }}">Comptes Syndicat</a>
              <a class="collapse-item {{ (request()->is('trashedSyndic')) ? 'active' : '' }}" href="{{ url('trashedSyndic') }}">Comptes Désactivé</a>
              <a class="collapse-item {{ (request()->is('inscriptionSyndic')) ? 'active' : '' }}" href="{{ url('inscriptionSyndic') }}">Demande d'inscription</a>
            </div>
          </div>
        </li>
        @endif
        @endif

        <!-- Copropriétaire -->
        @if(Auth::user()->status != "Syndic")
        @if(Auth::user()->status != "Coprop")
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsethree" aria-expanded="true" aria-controls="collapsethree">
            <i class="fas fa-fw fa-cog"></i>
              <span>Copropriétaire</span>
          </a>
          <div id="collapsethree" class="collapse" aria-labelledby="headingthree" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <h6 class="collapse-header">Opérations:</h6>
            <a class="collapse-item {{ (request()->is('coprop')) ? 'active' : '' }}" href="{{ url('coprop') }}">Comptes Copropriétaire</a>
              <a class="collapse-item {{ (request()->is('trashedCoprop')) ? 'active' : '' }}" href="{{ url('trashedCoprop') }}">Comptes Désactivé</a>
              <a class="collapse-item {{ (request()->is('inscriptionCorpro')) ? 'active' : '' }}" href={{ url('inscriptionCorpro') }}>Demande d'inscription</a>
            </div>
          </div>
        </li>
        
        <!-- Divider -->
        <hr class="sidebar-divider">
        
        @endif
        @endif
  
  
        
  
        <!-- Heading -->
        <div class="sidebar-heading">
          Gestion contenu
        </div>
  
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            
        @if(Auth::user()->status == "Admin" or Auth::user()->status == "Agent")
          <a class="nav-link collapsed" href="{{ url('actualite') }}" >
            <i class="fas fa-fw fa-folder"></i>
            <span>Actualité</span>
          </a>
        @endif
        
        @if(Auth::user()->status == "Syndic" or Auth::user()->status == "Coprop")
          <a class="nav-link collapsed" href="{{ url('listeActualite') }}" >
            <i class="fas fa-fw fa-folder"></i>
            <span>Actualité.</span>
          </a>
        @endif
          
          <a class="nav-link collapsed" href="{{ url('faq') }}" >
            <i class="fas fa-fw fa-folder"></i>
            <span>F.A.Q</span>
          </a>
          
          
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">
  
        <!-- Heading -->
        <div class="sidebar-heading">
          Réclamations
        </div>
  
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
            <i class="fas fa-fw fa-folder"></i>
            <span>Réclamations</span>
          </a>
          <div id="collapseFive" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <h6 class="collapse-header">Réclamation:</h6>

              @if(Auth::user()->status == "Admin" or Auth::user()->status == "Agent")
              <a class="collapse-item {{ (request()->is('recherche')) ? 'active' : '' }}" href="{{ url('recherche') }}">Recherche Réclamation</a>
              <a class="collapse-item {{ (request()->is('reclamation')) ? 'active' : '' }}" href="{{ url('reclamation') }}">Liste Réclamations</a>
              @endif

              <a class="collapse-item {{ (request()->is('MesReclamation')) ? 'active' : '' }}" href="{{ url('MesReclamation') }}">Mes Réclamations</a>
            </div>
          </div>
        </li>
        
        <!-- Divider -->
        <hr class="sidebar-divider">
  
        <!-- Heading -->
        <div class="sidebar-heading">
          Paiements
        </div>
  
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFive2" aria-expanded="true" aria-controls="collapseFive2">
            <i class="fas fa-fw fa-folder"></i>
            <span>Paiements</span>
          </a>
          <div id="collapseFive2" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <h6 class="collapse-header">Paiements:</h6>
                    
              @if(Auth::user()->status == "Admin" or Auth::user()->status == "Agent")
              <a class="collapse-item {{ (request()->is('getbalance')) ? 'active' : '' }}" href="{{ url('getbalance') }}">Balance</a>
              <a class="collapse-item {{ (request()->is('paiement')) ? 'active' : '' }}" href="{{ url('paiement') }}">Lancement Paiement</a>
              <a class="collapse-item {{ (request()->is('cotisation')) ? 'active' : '' }}" href="{{ url('cotisation') }}">Cotisations</a>
              <a class="collapse-item {{ (request()->is('ajoutPaie')) ? 'active' : '' }}" href="{{ url('ajoutPaie') }}">Ajouter paiement</a>
              <a class="collapse-item {{ (request()->is('demandePaie')) ? 'active' : '' }}" href="{{ url('demandePaie') }}">Demande paiement</a>
              @endif
              
              @if(Auth::user()->status == "Syndic" or Auth::user()->status == "Coprop")
              <a class="collapse-item {{ (request()->is('etatPaiement')) ? 'active' : '' }}" href="{{ url('etatPaiement') }}">Etat paiement (Us)</a>
              @endif
            </div>
          </div>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">
  
        @if(Auth::user()->status != "Syndic")
        @if(Auth::user()->status != "Coprop")
        <!-- Heading -->
        <div class="sidebar-heading">
          Paramètres
        </div>
  
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-folder"></i>
            <span>Paramètres</span>
          </a>
          <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <h6 class="collapse-header">Paramètres:</h6>
              <a class="collapse-item {{ (request()->is('ville')) ? 'active' : '' }}" href="{{ url('ville') }}">Ville</a>
              <a class="collapse-item {{ (request()->is('residence')) ? 'active' : '' }}" href="{{ url('residence') }}">Résidence</a>
              <a class="collapse-item {{ (request()->is('intervention')) ? 'active' : '' }}" href="{{ url('intervention') }}">Intervention</a>
              <a class="collapse-item {{ (request()->is('partenaire')) ? 'active' : '' }}" href="{{ url('partenaire') }}">Partenaires</a>
              <div class="collapse-divider"></div>
              <h6 class="collapse-header">Autres Paramètres:</h6>
              <a class="collapse-item" href="#">Autres</a>
            </div>
          </div>
        </li>

        <hr class="sidebar-divider d-none d-md-block">
        
        @endif
        @endif

        <!-- Divider -->
        
  
        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
          <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>
  
      </ul>