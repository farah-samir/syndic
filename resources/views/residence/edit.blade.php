@extends('layouts.master')

@section('content')

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Résidence </h1>
          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Modifier Résidence</h6>
                        </div>
                        <div class="card-body">
    
                                <form method="POST" action="{{ url('residence/'.$resid->id) }}">
                                    @method('PUT')
                                    @csrf
                           
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Résidence</label>
                                           <input type="text" class="form-control" name="nomResidence" value="{{ $resid->nomResidence }}">
                                           </div>
                                       </div>  
                                       
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Ville</label>
                                           <input type="text" class="form-control" name="ville" value="{{ $resid->ville }}">
                                           </div>
                                       </div>
                                       
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Quartier</label>
                                           <input type="text" class="form-control" name="quartie" value="{{ $resid->quartie }}">
                                           </div>
                                       </div>
                                       
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Type</label>
                                           <input type="text" class="form-control" name="type" value="{{ $resid->type }}">
                                           </div>
                                       </div>
                                       
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Nombre d'immeubles</label>
                                           <input type="number" class="form-control" name="nbrImmo" value="{{ $resid->nbrImmo }}">
                                           </div>
                                       </div>
                                       
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Nombre d'appartements par étage</label>
                                           <input type="text" class="form-control" name="nbrAppEtage" value="{{ $resid->nbrAppEtage }}">
                                           </div>
                                       </div>
                                       
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Nombre d'entrées</label>
                                           <input type="number" class="form-control" name="nbrEntre" value="{{ $resid->nbrEntre }}">
                                           </div>
                                       </div>
                                       
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Espace vert</label>
                                                <select name="espaceVert" class="form-control">
                                                    <option selected value="{{ $resid->espaceVert }}">{{ $resid->espaceVert }}</option>
                                                    <option value="Oui">Oui</option>
                                                    <option value="Non">Non</option>
                                                </select>
                                           </div>
                                       </div>
                                       
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Nombre Ascenseur</label>
                                           <input type="number" class="form-control" name="nbrAscenseur" value="{{ $resid->nbrAscenseur }}">
                                           </div>
                                       </div>
                                       
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Parcking</label>
                                           <input type="number" class="form-control" name="nbrPark" value="{{ $resid->nbrPark }}">
                                           </div>
                                       </div>
                                       
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Année de construction</label>
                                           <input type="month" class="form-control" name="annee" value="{{ $resid->annee }}">
                                           </div>
                                       </div>
                                    
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline-info btn-sm float-right">Modifier</button>
                                        </div>
                                    </form>
    
                        </div>
                    </div>
              
                </div>
    
            </div>
   

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    


  @endsection

