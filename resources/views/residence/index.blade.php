@extends('layouts.master')

@section('content')



        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Résidence</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">
                <i class="fas fa-address-card fa-sm text-white-50 mr-1"></i> Ajouter Résidence</a>
          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Liste Résidences</h6>
                            <a class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm float-right mr-2" href="{{ url('exportResidence') }}">
                                <i class="fas fa-download fa-sm text-white-50 mr-1"></i> Exporter</a>
                        </div>
                        <div class="card-body">
    
                            <nav aria-label="..." class="mt-5 float-right">
                                <ul class="pagination">
                                    {{ $residence->links() }} 
                                </ul>
                            </nav>
    
                            <table class="table table-bordered table-responsive-lg">
                                <thead>
                                    <tr>
                                        <th scope="col"><i class="fas fa-deaf"></i></th>
                                        <th scope="col">Résidence</th>
                                        <th scope="col">Type</th>
                                        <th scope="col">Ville</th>
                                        <th scope="col">Nbr.Immo</th>
                                        <th scope="col">Etage/Immo</th>
                                        <th scope="col">Nbr.Entré</th>
                                        <th scope="col">Espace Vert</th>
                                        <th scope="col">Etage Park</th>
                                        <th scope="col">Nbr.Ascenseur</th>
                                        <th scope="col">Année Constr</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @foreach($residence as $residences)
                                        <tr>
                                            <th scope="row">{{ $residences->id }}</th>
                                            <td>{{ strtoupper($residences->nomResidence) }}</td>
                                            <td>{{ $residences->type }}</td>
                                            <td>{{ $residences->ville }}</td>
                                            <td>{{ $residences->nbrImmo }}</td>
                                            <td>{{ $residences->nbrAppEtage }}</td>
                                            <td>{{ $residences->nbrEntre }}</td>
                                            <td>{{ $residences->espaceVert }}</td>
                                            <td>{{ $residences->nbrPark }}</td>
                                            <td>{{ $residences->nbrAscenseur }}</td>
                                            <td>{{ $residences->annee }}</td>
                                            <td>
                                                <div class="dropdown no-arrow">
                                                        
                                                    <form action="{{ url('residence/'.$residences->id) }}" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-outline-danger btn-sm float-right mr-2">Supprimer</button>
                                                    </form>
        
                                                    <a class="btn btn-outline-info btn-sm float-right mr-2" 
                                                    href="{{ url('residence/'.$residences->id.'/edit') }}">Modifier</a>
                                                    
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
    
    
                            <nav aria-label="..." class="mt-1 float-right" >
                                <ul class="pagination">
                                    {{ $residence->links() }} 
                                </ul>
                                
                            </nav>
    
                        </div>
                    </div>
              
                </div>
    
            </div>
   

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    
 
 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
             <h5 class="modal-title" id="exampleModalLabel">Ajouter nouvelle Résidence</h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
             </button>
         </div>
         <div class="modal-body" id="app">
         
         <form method="POST" action="{{ url('residence') }}">
         @csrf

            <div class="form-row">
                <div class="form-group col-lg-6">
                    <label class="col-form-label">Nom Résidence</label>
                    <input type="text" class="form-control" name="nomResidence" required>
                </div>

                <div class="form-group col-lg-3">
                    <label class="col-form-label">Ville</label>
                    <select name="ville" class="form-control" required>
                        <option selected disabled value="" >Ville...</option>
                        @foreach ($ville as $villes)
                        <option value="{{ $villes->ville }}">{{ $villes->ville }}</option> 
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-lg-3">
                    <label class="col-form-label">Quartier</label>
                    <input type="text" class="form-control" name="quartie" required>
                </div>
            </div>

            <residence-component></residence-component>
<!-- apres - choix cummin 2-->
            <div class="form-row">  
                <div class="form-group col-lg-4">
                    <label class="col-form-label">Nbr entrer</label>
                    <input type="number" class="form-control" name="nbrEntre" required>
                </div>

                <div class="form-group col-lg-4">
                    <label class="col-form-label">espace vert</label>
                    <select name="espaceVert" class="form-control">
                        <option selected value="Oui">Oui</option>
                        <option value="Non">Non</option>
                    </select>
                </div>

                <div class="form-group col-lg-4">
                    <label class="col-form-label">ascenseur</label>
                    <input type="number" class="form-control" name="nbrAscenseur" value="0" required>
                </div>
            </div>

            <div class="form-row">  
                <div class="form-group col-lg-6">
                    <label class="col-form-label">Nbr park</label>
                    <input type="number" class="form-control" name="nbrPark" value="0" required>
                </div>

                <div class="form-group col-lg-6">
                    <label class="col-form-label">Année construction</label>
                    <input type="month" class="form-control" name="annee" required>
                </div>
            </div>


             <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                 <button type="submit" class="btn btn-primary">Ajouter</button>
             </div>
         </form>
         
         </div>
 
      </div>
    </div>
</div>


  @endsection

