@extends('layouts.master')

@section('content')

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Coproprietaire </h1>
          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Modifier Coproprietaire</h6>
                        </div>
                        <div class="card-body">
    
                                <form method="POST" action="{{ url('coprop/'.$coprop->id) }}">
                                    @method('PUT')
                                    @csrf
                           
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Nom</label>
                                           <input type="text" class="form-control" name="name" value="{{ $coprop->name }}">
                                           </div>
                                       </div>  
                                       
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Prénom</label>
                                           <input type="text" class="form-control" name="prenom" value="{{ $coprop->prenom }}">
                                           </div>
                                       </div>
                                       
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Téléphone</label>
                                           <input type="text" class="form-control" name="telephone" value="{{ $coprop->telephone }}">
                                           </div>
                                       </div>
                                       
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">C.I.N</label>
                                           <input type="text" class="form-control" name="cin" value="{{ $coprop->cin }}">
                                           </div>
                                       </div>
                                       
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Adresse</label>
                                           <input type="text" class="form-control" name="adresse" value="{{ $coprop->adresse }}">
                                           </div>
                                       </div>
                                       
                                       <div class="form-row">
                                            <div class="form-group col-lg-6">
                                                <label for="exampleFormControlSelect2">Résidence</label>
                                                <select  class="form-control" name="residence">
                                                    
                                                    <option>{{ $coprop->residence }}</option>
                                                @foreach($residence as $res)
                                                    <option>{{ $res->nomResidence }}</option>
                                                @endforeach
                                                 
                                                </select>
                                            </div>
                                        </div>
                                       
                                       <div class="form-row">
                                            <div class="form-group col-lg-6">
                                                <label for="exampleFormControlSelect2">Ville</label>
                                                <select  class="form-control" name="ville">
                                                    
                                                    <option>{{ $coprop->ville }}</option>
                                                @foreach($ville as $vll)
                                                    <option>{{ $vll->ville }}</option>
                                                @endforeach
                                                 
                                                </select>
                                            </div>
                                        </div>
                                       
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Email</label>
                                           <input type="text" class="form-control" name="email" value="{{ $coprop->email }}">
                                           </div>
                                       </div>
                                    
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline-info btn-sm float-right">Modifier</button>
                                        </div>
                                    </form>
    
                        </div>
                    </div>
              
                </div>
    
            </div>
   

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    


  @endsection

