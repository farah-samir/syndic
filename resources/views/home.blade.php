@extends('layouts.master')

@section('content')



        <!-- Begin Page Content -->
        <div class="container-fluid">
        @if(Auth::user()->status == "Admin")
          <!-- Dashboard -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">ACCUEIL</h1>
          </div>
        @endif
          <!-- Content Row -->
          <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            @if(Auth::user()->status == "Admin")
            <div class="col- col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Admin</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $adminT }}</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col- col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Agent admin</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $agentT }}</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            

            <!-- Earnings (Monthly) Card Example -->
            <div class="col- col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Syndicat</div>
                      <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{ $syndicT }}</div>
                        </div>
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col- col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Copropriétaire</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $copropT }}</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-comments fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endif
          </div>

@if(Auth::user()->status == "Admin")

          <!-- Actualité -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">.</h1>
          </div>

@endif

@if(Auth::user()->status != "Admin")
          <!-- Actualité -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Actualités</h1>
            <a href="{{ url('listeActualite') }}" class=" d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-address-card fa-sm text-white-50 mr-1"></i> Voir liste</a>
          </div>

          <div class="row">
            <div class="col-lg-12">         
                <div class="card shadow mb-4">
                    <div class="card-body">
                      <div class="accordion" id="accordionExample">

                          @foreach ($actualite as $actualites)
                                        
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h2 class="mb-0">
                                              <button class="btn btn-link text-left" type="button" data-toggle="collapse" data-target="#collapse{{ $actualites->id }}" 
                                                                                                aria-expanded="true" aria-controls="collapse{{ $actualites->id }}">
                                                {{ $actualites->titre }} 
                                              </button> 
                                            <span class="float-right" style="font-size:12px;">
                                                    Date publication : {{ date('d / m / Y', strtotime($actualites->created_at)) }} 
                                            </span>
    
                                            </h2>
                                        </div>
                                      
                                        <div id="collapse{{ $actualites->id }}" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                            @if(isset($actualites->image))
                                                                <img class="img-fluid" src="{{ url($actualites->image) }}"  /> 
                                                            @else
                                                                <img class="img-fluid" src="{{ asset('assets/img/no-image.png') }}" /> 
                                                            @endif
                                                            
                                                        </div>
                                                    <div class="col-md-8">
                                                            {!! $actualites->ckeditor !!}
                                                    </div>
                                                </div>
                                                    
                                            </div>
                                        </div>
                                    </div>
    
                          @endforeach
    
                      </div>
                    </div>
                </div>
            </div>
        </div>
@endif
                    
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


  @endsection

