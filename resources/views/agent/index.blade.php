@extends('layouts.master')

@section('content')



        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Agent Admin</h1>
            <a href="#" class="btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">
                <i class="fas fa-address-card fa-sm text-white-50 mr-1"></i> Ajouter Agent Admin</a>
          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Liste Agent Admin</h6>
                        </div>
                        <div class="card-body">
    
                            <nav aria-label="..." class="mt-5 float-right">
                                <ul class="pagination">
                                    {{ $agent->links() }} 
                                </ul>
                            </nav>
    
                            <table class="table table-bordered table-responsive-lg">
                                <thead>
                                    <tr>
                                        <th scope="col"><i class="fas fa-deaf"></i></th>
                                        <th scope="col">Nom & Prénom</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Téléphone</th>
                                        <th scope="col">date de création</th>
                                        <th scope="col">Etat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @foreach($agent as $agents)
                                        <tr>
                                            <th scope="row">{{ $agents->id }}</th>
                                            <td>{{ $agents->name }} {{ $agents->prenom }}</td>
                                            <td>{{ $agents->email }}</td>
                                            <td>{{ $agents->telephone }}</td>
                                            <td>{{ date('d / m / Y', strtotime($agents->created_at))}}</td>
                                            <td>
                                                
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <form action="{{ url('agent/'.$agents->id) }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                        <button type="submit" class="btn btn-outline-danger btn-sm mr-1">Désactiver</button>
                                                    </form>
                                                    
                                                  <a href="{{ url('agent/'.$agents->id.'/edit') }}" class="btn btn-outline-info btn-sm">Modifier</a>
                                                </div>

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
    
    
                            <nav aria-label="..." class="mt-1 float-right" >
                                <ul class="pagination">
                                    {{ $agent->links() }} 
                                </ul>
                            </nav>
    
                        </div>
                    </div>
              
                </div>
    
            </div>
   

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    
 
 
 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
             <h5 class="modal-title" id="exampleModalLabel">Ajouter Agent Admin</h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
             </button>
         </div>
         <div class="modal-body">
         
            <form class="user" method="POST" action="{{ url('agent') }}">
                @csrf
                <div class="col-sm-3 offset-sm-1 mb-3 mb-sm-0">
                    <input class="form-check-input" type="hidden" name="dateA" value="{{ date('Y-m-d H:i:s') }}" >
                  </div>

                <div class="form-group row">
                    <div class="col-sm-3 offset-sm-1 mb-3 mb-sm-0">
                      <input class="form-check-input" id="exampleCheck11" type="radio" name="genre" value="Madame" checked>
                      <label class="form-check-label" for="exampleCheck11">Madame</label>
                    </div>
  
                    <div class="col-sm-3 mb-3 mb-sm-0">
                      <input class="form-check-input" id="exampleCheck10" type="radio" name="genre" value="Monsieur" checked>
                      <label class="form-check-label" for="exampleCheck10">Monsieur</label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                      <input type="text" class="form-control form-control-user" name="name" value="{{ old('name') }}" placeholder="Nom" required autocomplete="name" autofocus>
                    </div>
                    <div class="col-sm-6">
                      <input type="text" class="form-control form-control-user" name="prenom" value="{{ old('prenom') }}" placeholder="Prénom" required autocomplete="prenom" autofocus>
                    </div>
                </div>

                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" name="telephone" placeholder="Téléphone" required autocomplete="telephone" autofocus>
                  </div>
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" name="cin" placeholder="CIN" required autocomplete="cin" autofocus>
                  </div>
                </div>


                <div class="form-group">
                  <input type="text" class="form-control form-control-user" name="adresse" placeholder="Adresse"  autocomplete="adresse" autofocus>
                </div>

                <div class="form-group row">
                    <div class="col-sm-12 mb-3 mb-sm-0">
                      <input type="text" class="form-control form-control-user" name="ville" placeholder="Ville"  autocomplete="ville" autofocus>
                    </div>
                </div>

                <hr>
                <div class="form-group row">
                    <div class="col-sm-12 offset-sm-1 mb-3 mb-sm-0">
                        <input class="form-check-input" id="exampleCheck1" type="radio" name="status" value="Agent" checked>
                        <label class="form-check-label" for="exampleCheck1">Agent Admin</label>
                    </div>
                </div>
                <hr>

                <div class="form-group">
                  <input type="email" class="form-control form-control-user" name="email" value="{{ old('email') }}" placeholder="Email" required autocomplete="email">
                </div>

                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="password" class="form-control form-control-user" name="password" placeholder="Mot de passe" required autocomplete="new-password">
                  </div>
                  <div class="col-sm-6">
                    <input type="password" class="form-control form-control-user" name="password_confirmation" placeholder="Confirmer Mot de passe" required autocomplete="new-password">
                  </div>
                </div>

                <button type="submit" class="btn btn-primary btn-user btn-block">
                  Valider
                </button>

            </form>
         
         </div>
 
      </div>
    </div>
</div>

  @endsection

