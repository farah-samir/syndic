@extends('layouts.master')

@section('content')



        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Partenaires</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm " 
                                                data-toggle="modal" 
                                                data-target="#exampleModal" 
                                                data-whatever="@getbootstrap">
                <i class="fas fa-address-card fa-sm text-white-50 mr-1"></i> Ajouter Partenaire</a>
          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Liste Partenaire</h6>
                        </div>
                        <div class="card-body">
    
                            <nav aria-label="..." class="mt-5 float-right">
                                <ul class="pagination">
                                    {{ $partenaire->links() }} 
                                </ul>
                            </nav>
    
                            <table class="table table-bordered table-responsive-lg">
                                <thead>
                                    <tr>
                                        <th scope="col"><i class="fas fa-deaf"></i></th>
                                        <th scope="col">Nom</th>
                                        <th scope="col">Domaine d'intervention</th>
                                        <th scope="col">Ville</th>
                                        <th scope="col">Téléphone</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @foreach($partenaire as $partenaires)
                                        <tr>
                                            <th scope="row">{{ $partenaires->id }}</th>
                                            <td>{{ $partenaires->nom }}</td>
                                            <td>{{ $partenaires->domaine }}</td>
                                            <td>{{ $partenaires->ville }}</td>
                                            <td>{{ $partenaires->tel }}</td>
                                            <td>{{ $partenaires->email }}</td>
                                            <td>
                                                <div class="dropdown row">
                                                    <form action="{{ url('partenaire/'.$partenaires->id) }}" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-outline-danger btn-sm mr-2">Supprimer</button>
                                                    </form>
                                                        
                                                    <a class="btn btn-outline-info btn-sm" 
                                                    href="{{ url('partenaire/'.$partenaires->id.'/edit') }}">Modifier</a>
                                                    
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
    
    
                            <nav aria-label="..." class="mt-1 float-right">
                                <ul class="pagination">
                                    {{ $partenaire->links() }} 
                                </ul>
                            </nav>
    
                        </div>
                    </div>
              
                </div>
    
            </div>
   

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    
 
 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
             <h5 class="modal-title" id="exampleModalLabel">Ajouter Nouveau Partenaire</h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
             </button>
         </div>
         <div class="modal-body">
         
         <form method="POST" action="{{ url('partenaire') }}">
         @csrf
 
             <div class="form-group">
                 <label class="col-form-label">Nom</label>
                 <input type="text" class="form-control" id="ville" name="nom" required>
             </div>

             <div class="form-group">
                    <label for="exampleFormControlSelect2">Domaine d'intervention</label>
                <select class="form-control" name="domaine">
                    @foreach($intervention as $interventions)
                        <option>{{ $interventions->intervention }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label class="col-form-label">Téléphone</label>
                <input type="tel" class="form-control" id="ville" name="tel" required>
            </div>

            <div class="form-group">
                <label class="col-form-label">Email</label>
                <input type="email" class="form-control" id="ville" name="email" required>
            </div>

            <div class="form-group">
                <label class="col-form-label">Ville</label>
                <select name="ville" class="form-control" required>
                    <option selected disabled value="" >Ville...</option>
                    @foreach ($ville as $villes)
                    <option value="{{ $villes->ville }}">{{ $villes->ville }}</option> 
                    @endforeach
                </select>
            </div>
          
            <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                 <button type="submit" class="btn btn-primary">Ajouter</button>
            </div>
         </form>
         
         </div>
 
      </div>
    </div>
</div>

  @endsection

