@extends('layouts.master')

@section('content')

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Partenaire </h1>
          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Modifier Partenaire</h6>
                        </div>
                        <div class="card-body">
    
                                <form method="POST" action="{{ url('partenaire/'.$part->id) }}">
                                    @method('PUT')
                                    @csrf
                           
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Nom</label>
                                           <input type="text" class="form-control" name="nom" value="{{ $part->nom }}">
                                           </div>
                                       </div>  
                                       
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Prénom</label>
                                           <input type="text" class="form-control" name="domaine" value="{{ $part->domaine }}">
                                           </div>
                                       </div>
                                       
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Téléphone</label>
                                           <input type="text" class="form-control" name="tel" value="{{ $part->tel }}">
                                           </div>
                                       </div>
                                       
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Ville</label>
                                           <input type="text" class="form-control" name="ville" value="{{ $part->ville }}">
                                           </div>
                                       </div>
                                       
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Email</label>
                                           <input type="email" class="form-control" name="email" value="{{ $part->email }}">
                                           </div>
                                       </div>
                                    
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline-info btn-sm float-right">Modifier</button>
                                        </div>
                                    </form>
    
                        </div>
                    </div>
              
                </div>
    
            </div>
   

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    


  @endsection

