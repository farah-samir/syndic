@extends('layouts.master')

@section('content')



        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Recherche résidence</h1>
          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Cotisation</h6>
                            <button class="d-sm-inline-block btn btn-sm btn-primary shadow-sm float-right mr-2" data-toggle="modal" data-target="#exampleModal">
                                <i class="fas fa-download fa-sm text-white-50 mr-1"></i> Augmentation</button>
                        </div>
                        <div class="card-body">
    
                                <form class="needs-validation" action="{{ url('foundCotisation') }}" method="post">
                                    @csrf
                                    <div class="form-row">
        
                                        <div class="col-md-3 mb-3">
                                            <select class="form-control myselect" name="residence">
                                                <option disabled selected>Résidence</option>
                                                @foreach($residence as $residences) 
                                                    <option value="{{ $residences->nomResidence}}">{{ $residences->nomResidence}}</option>
                                                @endforeach
                                            </select>
        
                                            <script type="text/javascript">
                                                $(".myselect").select2();
                                            </script> 
        
                                            <div class="valid-tooltip">
                                                Looks good!
                                            </div>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <button class="btn-shadow btn btn-info btn-sm" type="submit">Recherche</button>
                                        </div>
                                        
                                    </div>
                                </form>
    
                        </div>
                    </div>
              
                </div>
    
            </div>

            <div class="row">
                <div class="col-md-12">

                    @if(isset($coprop))
                        
                        <div class="row">
                            <div class="col-lg-12">
                          
                                <div class="card shadow mb-4">
                
                                    <div class="card-header py-3">
                                        

                                        <h6 class="m-0 font-weight-bold text-primary">Liste </h6>
                                    </div>
                                    <div class="card-body">
                
                                        <nav aria-label="..." class="mt-5 float-right">
                                            <ul class="pagination">
                                                link
                                            </ul>
                                        </nav>
                
                                        <table class="table table-bordered table-responsive-lg">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Nom et prénom</th>
                                                    <th scope="col">Téléphone</th>
                                                    <th scope="col">Résidence</th>
                                                    <th scope="col">Cotisation</th>
                                                    <th scope="col">Solde Ini</th>
                                                    <th scope="col" class="text-right mr-2">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                @foreach($coprop as $coprops)
                                                    
                                                    <tr>
                                                        <td>{{ $coprops->name }} {{ $coprops->prenom }}</td>
                                                        <td>{{ $coprops->telephone }}</td>
                                                        <td>{{ $coprops->residence }}</td>
                                                        <td>
                                                            @if(isset($coprops->cotisation))
                                                                
                                                                <span class="btn-outline-success">{{ $coprops->cotisation }}</span>
                                                            @else
                                                                <span class="btn-outline-danger">None</span>
                                                            @endif
                                                        </td>
                                                        <td> 
                                                            <span class="btn-outline-success"> {{ $coprops->solde }} </span>
                                                        </td>
                                                        <td>
                                                            <div class="dropdown no-arrow">
                                                                
                                                                <a href="{{ url('etatPaie/'.$coprops->id) }}" class="btn btn-outline-info btn-sm float-right mr-2" >
                                                                  Etat Paiement
                                                                </a>
                                                                
                                                                <button type="button" class="btn btn-outline-success btn-sm float-right mr-2" 
                                                                                        data-toggle="modal" data-target="#exModal{!!$coprops->id!!}">
                                                                  Cotisation
                                                                </button>
                                                                
                                                                <button type="button" class="btn btn-outline-warning btn-sm float-right mr-2" 
                                                                                        data-toggle="modal" data-target="#exModalx{!!$coprops->id!!}">
                                                                  Solde Initial
                                                                </button>
      

<!-- Modal cotisation -->
<div class="modal fade" id="exModal{!!$coprops->id!!}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">

    <form class="form-horizontal" action="{{ url('cotisation/'.$coprops->id) }}" method="post">
        @method('PUT')
        @csrf      
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cotisation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body">
        Prix de cotisation pour : <span class="text-success">{{ $coprops->name }} {{ $coprops->prenom }}</span>
      </div>
      
      <div class="modal-body">
        <input type="hidden" class="form-control" value="{{ $coprops->id }}">
        <input type="hidden" class="form-control" value="{{ $coprops->residence }}" name="residence">
        <input type="number" class="form-control" value="{{ $coprops->cotisation }}" name="cotisation">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-primary btn-sm">Valider</button>
      </div>
    </div>
    
    </form>
    
  </div>
</div> 
<!-- Modal cotisation FIN -->

<!-- Modal solde initial -->
<div class="modal fade" id="exModalx{!!$coprops->id!!}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">

    <form class="form-horizontal" action="{{ url('solde/'.$coprops->id) }}" method="post">
        @method('PUT')
        @csrf      
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Solde initial</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body">
        Solde credit : <span class="text-success">{{ $coprops->name }} {{ $coprops->prenom }}</span>
      </div>
      
      <div class="modal-body">
        <input type="hidden" class="form-control" value="{{ $coprops->id }}">
        <input type="hidden" class="form-control" value="{{ $coprops->residence }}" name="residence">
        <input type="number" class="form-control" value="{{ $coprops->solde }}" name="solde" placeholder="Solde initial">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-primary btn-sm">Valider</button>
      </div>
    </div>
    
    </form>
    
  </div>
</div> 
<!-- Modal solde initial FIN -->
                                                                
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                
                                        <nav aria-label="..." class="mt-1 float-right">
                                            <ul class="pagination">
                                                link
                                            </ul>
                                        </nav>
                
                                    </div>
                                
                                </div>
                          
                            </div>
                
                        </div>
                    @endif
                  





                </div>
            </div>
   
            <div class="row">
                
            </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

  
 <!-- Modal Augmentation -->
 
 <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Augmentation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
        <form class="needs-validation" action="{{ url('updateCota') }}" method="post">
            @csrf
        <div class="modal-body">
            <div class="form-row">
                <div class="col-md-12 mb-3">
                    <select class="form-control myselect" id="myselect" name="residence" required>
                        <option disabled selected value="">Résidence</option>
                        @foreach($residence as $residences) 
                        <option value="{{ $residences->nomResidence}}">{{ $residences->nomResidence}}</option>
                        @endforeach
                    </select>
        
                    <script type="text/javascript">
                        $("#myselect").select2({ width: '100%' });
                    </script> 
        
                    <div class="valid-tooltip">
                        Looks good!
                    </div>
                </div> 
            </div>
  
          <div class="form-group">
            <label for="message-text" class="col-form-label">Augmentation de (Dh):</label>
            <input type="number" class="form-control" name="cota" required>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-primary">Valider</button>
      </div>
      </form>
    </div>
  </div>
</div>

 <!-- Modal Augmentation -->

  @endsection

