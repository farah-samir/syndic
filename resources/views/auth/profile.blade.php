@extends('layouts.master')

@section('content')

<style>
  .dums{
    height: 100px;
  }
</style>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Profile</h1>
          </div>
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Profile</h6>
                        </div>
                        <div class="card-body">
    
                            
                                <div class="container" style="padding-top: 60px;">
                                        <h1 class="page-header">Edit Profile</h1>
                                        <div class="row">
                                          <!-- left column -->
                                          
                                          <!-- edit form column -->
                                          <div class="col-md-8 col-sm-6 col-xs-12 personal-info">
                                            @if (session('status'))
                                              <div class="alert alert-info alert-dismissable">
                                                <a class="panel-close close" data-dismiss="alert">×</a> 
                                                <i class="fa fa-coffee"></i>
                                                {{ session('status') }}.
                                              </div>
                                            @endif

                                            <h3>Personal info</h3>
                                            <form class="form-horizontal" action="{{ url('profile/'.Auth::user()->id) }}" method="post" enctype="multipart/form-data">
                                              @method('PUT')
                                              @csrf

                                              <div class="form-group">
                                                  <div class="col-lg-8">
                                                      <div class="text-center">
                                                      
                                                        @if(isset(Auth::user()->image))
                                                                <img src="{{ asset(Auth::user()->image) }}" class="img-thumbnail" style="height:200px;"> 
                                                        @else
                                                                <img class="img-fluid" src="{{ asset('assets/img/no-image.png') }}" /> 
                                                        @endif
                                                          <label for="upload">
                                                              <span id="preview"></span>
                                                          </label>
          
                                                          <h4>{{ strtoupper(Auth::user()->name) }} {{ strtoupper(Auth::user()->prenom) }}</h4>
          
                                                          <div class="custom-file">
                                                              <input type="file" onchange="handleFiles(files)" id="upload" name="image" >
                                                              <label class="custom-file-label" for="upload">Selectionner Image</label>
                                                          </div>
          
                                                      </div>
                                                    </div>
                                              </div>

                                              <div class="form-group">
                                                <label class="col-lg-3 control-label">Nom:</label>
                                                <div class="col-lg-8">
                                                  <input class="form-control" value="{{ Auth::user()->name }}" type="text" name="name">
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <label class="col-lg-3 control-label">Prénom:</label>
                                                <div class="col-lg-8">
                                                  <input class="form-control" value="{{ Auth::user()->prenom }}" type="text" name="prenom">
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <label class="col-lg-3 control-label">Status:</label>
                                                <div class="col-lg-8">
                                                  <input class="form-control" value="{{ Auth::user()->status }}" type="text" disabled>
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <label class="col-md-3 control-label">Résidence:</label>
                                                <div class="col-md-8">
                                                  <input class="form-control" value="{{ Auth::user()->residence }}" type="text" disabled>
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <label class="col-md-12 control-label">Complément adresse:</label>
                                                <div class="col-md-8">
                                                  <input class="form-control" value="{{ Auth::user()->adresse }}" type="text" name="adresse">
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <label class="col-lg-3 control-label">Email:</label>
                                                <div class="col-lg-8">
                                                  <input class="form-control" value="{{ Auth::user()->email }}" type="text" name="email" disabled>
                                                </div>
                                              </div>
                                              
                                              <div class="form-group">
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-8">
                                                  <button class="btn btn-primary" type="submit">Mettre à jour</button>
                                                </div>
                                              </div>
                                            </form>
                                              <div>
                                                <a href="{{ route('password.request') }}" class="btn btn-warning">Modifier votre mot de passe</a>
                                              </div>
                                          </div>
                                        </div>
                                      </div>
                            
    
                        </div>
                    </div>
              
                </div>
    
            </div>
   

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    
<script>
    function handleFiles(files) {
      var imageType = /^image\//;
      for (var i = 0; i < files.length; i++) {
      var file = files[i];
      if (!imageType.test(file.type)) {
        alert("veuillez sélectionner une image");
      }else{
        if(i == 0){
          preview.innerHTML = '';
        }
        var img = document.createElement("img");
        img.classList.add("avatar");
        img.classList.add("img-circle");
        img.classList.add("img-thumbnail");
        img.classList.add("dums");
        img.file = file;

        preview.appendChild(img); 
        var reader = new FileReader();
        reader.onload = ( function(aImg) { 
        return function(e) { 
        aImg.src = e.target.result; 
      }; 
     })(img);
   
    reader.readAsDataURL(file);
    }
    
    }
   }
</script>

  @endsection

