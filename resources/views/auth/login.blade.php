@extends('layouts.template')

@section('content')

<body class="bg-gradient-primary bg">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-black text-center">
                <img src="{{ asset('assets/img/syndic-city.jpg') }}">
              </div>
              <div class="col-lg-6 card border-left-primary shadow h-100 py-2">
                <div class="p-5">
                    @if (session('status'))
                        <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
                    @endif
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Veuillez vous identifier</h1>
                  </div>
                  <form class="user" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                      <input type="email"   class="form-control form-control-user @error('email') is-invalid @enderror" 
                                            name="email" value="{{ old('email') }}" 
                                            required autocomplete="email" 
                                            autofocus placeholder="Entrer votre adresse...">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                    </div>
                    <div class="form-group">
                      <input type="password"    class="form-control form-control-user @error('password') is-invalid @enderror" 
                                                name="password" 
                                                required autocomplete="current-password" 
                                                placeholder="Mot de passe">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label" for="customCheck">Souviens-toi de moi</label>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block">
                      S'identifier
                    </button>
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="small" href="{{ route('password.request') }}">Mot de passe oublié ?</a>
                  </div>
                  <div class="text-center">
                    <a href="{{ url('inscription') }}" class="btn btn-warning">Pas encore de compte ? Créer un compte</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  @endsection
