@extends('layouts.template')

@section('content')

<body class="bg-gradient-primary bg">

  <div class="container" >

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-black text-center">
            <img src="{{ asset('assets/img/syndic-city.jpg') }}">
          </div>
          <div class="col-lg-7 card border-left-primary shadow h-100 py-2" id="app">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Créer votre compte</h1>
              </div>
                <!--<form class="user" method="POST" action="{{ route('register') }}">-->
                  <form class="user" method="POST" action="{{ url('demmande') }}">  
                @csrf
                  
                <div class="form-group row">
                  <div class="col-sm-3 offset-sm-1 mb-3 mb-sm-0">
                    <input class="form-check-input" id="exampleCheck11" type="radio" name="genre" value="Madame" checked>
                    <label class="form-check-label" for="exampleCheck11">Madame</label>
                  </div>

                  <div class="col-sm-3 mb-3 mb-sm-0">
                    <input class="form-check-input" id="exampleCheck10" type="radio" name="genre" value="Monsieur" checked>
                    <label class="form-check-label" for="exampleCheck10">Monsieur</label>
                  </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                      <input type="text" class="form-control form-control-user" name="name" value="{{ old('name') }}" placeholder="Nom" required autocomplete="name" autofocus>
                    </div>
                    <div class="col-sm-6">
                      <input type="text" class="form-control form-control-user" name="prenom" value="{{ old('prenom') }}" placeholder="Prénom" required autocomplete="prenom" autofocus>
                    </div>
                </div>

                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" name="telephone" value="{{ old('telephone') }}" placeholder="Téléphone" 
                    pattern="[0]{1}[6,5]{1}[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}" required autocomplete="telephone" autofocus>
                  </div>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-user" name="titreFoncier" value="{{ old('titreFoncier') }}" placeholder="N titre foncier"  autocomplete="titreFoncier" autofocus>
                  </div>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-user" name="cin" value="{{ old('cin') }}" placeholder="CIN"  autocomplete="cin" autofocus>
                  </div>
                </div>

                <example-component></example-component>

                <div class="form-group row">
                  <div class="col-sm-12 mb-sm-0">
                    <input type="text" class="form-control form-control-user" name="adresse" value="{{ old('adresse') }}" placeholder="N° Imm - N° Appt" required autocomplete="adresse" autofocus>
                  </div>
                </div>

                <hr>
                <div class="form-group row">
                    <div class="col-sm-12 offset-sm-1 mb-3 mb-sm-0">
                        <input class="form-check-input" id="exampleCheck1" type="radio" name="status" value="Coprop" checked>
                        <label class="form-check-label" for="exampleCheck1">Je suis locataire</label>
                    </div>
                    <div class="col-sm-12 offset-sm-1 mb-3 mb-sm-0">
                        <input class="form-check-input" id="exampleCheck2" type="radio" name="status" value="Coprop" checked>
                        <label class="form-check-label" for="exampleCheck2">Je suis propriétaire</label>
                    </div>
                    <div class="col-sm-12 offset-sm-1 mb-3 mb-sm-0">
                        <input class="form-check-input" id="exampleCheck3" type="radio" name="status" value="Syndic" checked>
                        <label class="form-check-label" for="exampleCheck3">Je suis propriétaire et membre du Conseil Syndical</label>
                    </div>
                    <div class="col-sm-12 offset-sm-1 mb-3 mb-sm-0">
                        <input class="form-check-input" id="exampleCheck4" type="radio" name="status" value="Syndic" checked>
                        <label class="form-check-label" for="exampleCheck4">Je suis propriétaire et président du Conseil Syndical</label>
                    </div>
                </div>
                
                <hr>

                <div class="form-group">
                  <input type="text" class="form-control form-control-user" name="email" value="{{ old('email') }}" placeholder="Email Ou Pseudo" required autocomplete="email">
                  <small class="form-text text-muted text-center">Votre mot de passe doit comporter entre 8 et 15 caractères</small>
                </div>
                  @error('email')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                <div class="form-group row">
                    
                  <div class="col-sm-6 mb-3 mb-sm-0">
                      
                    <input type="password" class="form-control form-control-user" 
                            name="password" placeholder="Votre mot de passe" required autocomplete="new-password" minlength="8">
                  </div>
                  <div class="col-sm-6">
                    <input type="password" class="form-control form-control-user" name="password_confirmation" placeholder="Confirmer Mot de passe" required autocomplete="new-password">
                  </div>
                </div>

                <hr>

                <button type="submit" class="btn btn-primary btn-user btn-block">
                  S'inscrire
                </button>
                <a href="{{ route('login') }}" class="btn btn-warning btn-user btn-block">
                  S'identifier
                </a>
                

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  @endsection
