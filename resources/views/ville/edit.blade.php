@extends('layouts.master')

@section('content')

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Villes </h1>
          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Modifier ville</h6>
                        </div>
                        <div class="card-body">
    
                                <form method="POST" action="{{ url('ville/'.$ville->id) }}">
                                    @method('PUT')
                                    @csrf
                           
                                       <div class="form-row">
                                           <div class="form-group col-lg-6">
                                               <label class="col-form-label">Ville</label>
                                           <input type="text" class="form-control" name="ville" value="{{ $ville->ville }}">
                                           </div>
                                       </div>        
                                    
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline-info btn-sm float-right">Modifier</button>
                                        </div>
                                    </form>
    
                        </div>
                    </div>
              
                </div>
    
            </div>
   

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    


  @endsection

