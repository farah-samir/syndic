@extends('layouts.master')

@section('content')



        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Villes</h1>
            <a href="#" class=" d-sm-inline-block btn btn-sm btn-primary shadow-sm " 
                                                data-toggle="modal" 
                                                data-target="#exampleModal" 
                                                data-whatever="@getbootstrap">
                <i class="fas fa-address-card fa-sm text-white-50 mr-1"></i> Ajouter Ville</a>
          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Liste Villes</h6>
                            <a class="d-sm-inline-block btn btn-sm btn-primary shadow-sm float-right mr-2" href="{{ url('exportVille') }}">
                                <i class="fas fa-download fa-sm text-white-50 mr-1"></i> Exporter</a>
                        </div>
                        <div class="card-body">
    
                            <nav aria-label="..." class="mt-5 float-right">
                                <ul class="pagination">
                                    {{ $ville->links() }} 
                                </ul>
                            </nav>
    
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col"><i class="fas fa-deaf"></i></th>
                                        <th scope="col">Ville</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @foreach($ville as $villes)
                                        <tr>
                                            <th scope="row">{{ $villes->id }}</th>
                                            <td>{{ strtoupper($villes->ville) }}</td>
                                            <td>
                                                <div class="dropdown no-arrow">
                                                        
                                                    <form action="{{ url('ville/'.$villes->id) }}" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-outline-danger btn-sm float-right mr-2">Supprimer</button>
                                                    </form>
        
                                                    <a class="btn btn-outline-info btn-sm float-right mr-2" 
                                                    href="{{ url('ville/'.$villes->id.'/edit') }}">Modifier</a>
                                                           
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
    
    
                            <nav aria-label="..." class="mt-1 float-right">
                                <ul class="pagination">
                                    {{ $ville->links() }} 
                                </ul>
                            </nav>
    
                        </div>
                    </div>
              
                </div>
    
            </div>
   

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    
 
 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
             <h5 class="modal-title" id="exampleModalLabel">Ajouter Nouvelle ville</h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
             </button>
         </div>
         <div class="modal-body">
         
         <form method="POST" action="{{ url('ville') }}">
         @csrf
 
             <div class="form-group">
                 <label class="col-form-label">Ville</label>
                 <input type="text" class="form-control" id="ville" name="ville" required>
             </div>
          
             <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                 <button type="submit" class="btn btn-primary">Ajouter</button>
             </div>
         </form>
         
         </div>
 
      </div>
    </div>
</div>

  @endsection

