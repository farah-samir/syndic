@extends('layouts.master')

@section('content')



        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Lancement du paiement</h1>
          </div>
          
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
            
            @if (session('sta'))
                <p class="mt-3 mb-3 mb-0 alert alert-danger">{{ session('sta') }}</p>
            @endif
            
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Lancement du paiement de l'année</h6>
                        </div>
                        <div class="card-body">
    
                                <form class="needs-validation" action="{{ url('paiement') }}" method="post">
                                    @csrf
                                    <div class="form-row">
        
                                        <div class="col-md-6">
                                            <!--<input type="month" class="form-control" name="mois" required>-->
                                            <input type="number" class="form-control" name="mois" required placeholder="Année format: aaaa"  maxlength="4">
                                        </div>

                                        <div class="col-md-6 mt-1">
                                            <button class="btn-shadow btn btn-info btn-sm" type="submit">Lancer</button>
                                        </div>
                                        
                                    </div>
                                </form>
    
                        </div>
                    </div>
              
                </div>
    
            </div>

            <div class="row">
                <div class="col-md-12">

                   
                        
                        <div class="row">
                            <div class="col-lg-12">
                          
                                <div class="card shadow mb-4">
                
                                    <div class="card-header py-3">
                                        

                                        <h6 class="m-0 font-weight-bold text-primary">Liste </h6>
                                    </div>
                                    <div class="card-body">
                
                                        <nav aria-label="..." class="mt-5 float-right">
                                            <ul class="pagination">
                                                link
                                            </ul>
                                        </nav>
                
                                        <table class="table table-bordered table-responsive-lg">
                                            <thead>
                                                <tr>
                                                    <th scope="col">mois</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($paie as $paies)
                                                    <tr>
                                                        <th scope="row"><i class="fas fa-fw fa-folder"></i> {{ $paies->mois }}</th>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                
                                        <nav aria-label="..." class="mt-1 float-right">
                                            <ul class="pagination">
                                                link
                                            </ul>
                                        </nav>
                
                                    </div>
                                
                                </div>
                          
                            </div>
                
                        </div>
             


                </div>
            </div>
   
            <div class="row">
                
            </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

  

  @endsection

