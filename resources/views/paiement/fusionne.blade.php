@extends('layouts.master')

@section('content')



        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">P Facture</h1>

          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                     
    
                        <div class="card-header py-3">
    
                            <h6 class="m-0 font-weight-bold text-primary">P facture</h6>
                        </div>
                        <div class="card-body">
    
                                
    
                           
                                <div class="row">
                                        <div class="col-md-12">
                                        <form action="{{ url('demande') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        
                                        <input type="hidden" name="user_id" value="{{ $user_id }}">
                                        <input type="hidden" name="mois" value="@foreach ($record as $records){{ $records->mois }}, @endforeach">
                                        <input type="hidden" name="montant" value="{{ $tl }}">
                                        <input type="hidden" name="recu" value="@foreach ($record as $records){{ $records->id }}, @endforeach">
                                        
                                        <div class="form-group">
                                            <label for="exampleInputPassword1"><strong> {{ Auth::user()->name }} {{ Auth::user()->prenom }} </strong></label>
                                        </div>
                                        
                                        <div class="form-group col-md-12">
                                            <hr>
                                        </div>
                                        
                                        <div class="form-row">
                                        @foreach ($record as $records)
                                        <div class="form-group col-md-4">
                                            <label for="exampleInputPassword1"><i class="fas fa-fw fa-folder"></i> Reçu: {{ $records->id }}</label>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="exampleInputPassword1">Mois: {{ $records->mois }}</label>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="exampleInputPassword1">Montant: {{ $records->cotisation }} MAD</label>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <hr>
                                        </div>
                                        @endforeach
                                        </div>
                                        
                                        <div class="form-row">
                                            <div class="form-group col-md-4 offset-md-8">
                                                <label for="exampleInputPassword1">Total:<span class="text-success"> {{ $tl }} MAD </span> </label>
                                            </div>
                                        </div>

            
                                            <br><br>
            
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Mode de paiement</label>
                                                <select class="form-control" name="mode">
                                                    <option value="Chèque">Chèque</option>
                                                    <option value="Espèce">Espèce</option>
                                                    <option value="Virement">Virement bancaire</option>
                                                </select>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Pièce justificative</label>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="image" required>
                                                    <label class="custom-file-label" for="inputGroupFile02" aria-describedby="inputGroupFileAddon02">Sélectionner pièce justificative</label>
                                                </div>
                                            </div>
                
            
                                                <button type="submit" class="btn btn-primary">Valider</button>
                                            </form>
                                        </div>
                                    </div>
    
                            
    
                        </div>
                     
                    </div>
              
                </div>
    
            </div>
    
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    
 

  @endsection

