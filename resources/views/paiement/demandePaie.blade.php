@extends('layouts.master')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Demande paiement</h1>
         </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
            
        <!-- Content Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-12">
                          
                        <div class="card shadow mb-4">
                
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Liste Demandes</h6>
                            </div>
                            
                            <div class="card-body">
                
                                <nav aria-label="..." class="mt-5 float-right">
                                    <ul class="pagination">
                                        link
                                    </ul>
                                </nav>
                
                            <table class="table table-bordered table-responsive-lg">
                                <thead>
                                    <tr>
                                        <th scope="col">Nom et prénom</th>
                                        <th scope="col">Mois</th>
                                        <th scope="col">Reçu</th>
                                        <th scope="col">Mode Paie</th>
                                        <th scope="col" class="text-right mr-2">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                                
                                    @foreach($demandes as $demande)
                                    <tr>
                                        <td>{{ $demande->user->name }} {{ $demande->user->prenom }}</td>
                                        <td>{{ $demande->mois }}</td>
                                        <td>{{ $demande->recu }}</td>
                                        <td><span class="btn-outline-success">{{ $demande->mode }}</span></td>
                                        <td>
                                            <div class="dropdown no-arrow">
                                                                
                                                <a href="{{ url('etatPaiex/'.$demande->id) }}" class="btn btn-outline-primary btn-sm float-right mr-2" >
                                                    Fermer la demande
                                                </a>
                                                
                                                <a href="{{ url('voireDemande/'.$demande->id) }}" class="btn btn-outline-info btn-sm float-right mr-2" >
                                                    Voire
                                                </a>
                                                
                                                <a href="{{ url('etatPaie/'.$demande->user_id) }}" class="btn btn-outline-warning btn-sm float-right mr-2" >
                                                    Etat paiement
                                                </a>
                          
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                
                                <nav aria-label="..." class="mt-1 float-right">
                                    <ul class="pagination">
                                        link
                                    </ul>
                                </nav>
                
                            </div>
                        </div>
                    </div>
                </div>
                   
            </div>
        </div>
    </div>
        <!-- /.container-fluid -->

</div>
      <!-- End of Main Content -->


  @endsection

