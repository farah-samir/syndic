@extends('layouts.master')

@section('content')

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Demande de paiement </h1>
          </div>
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary"> {{ strtoupper($dem->user->name) }} {{ strtoupper($dem->user->prenom) }}</h6>
                        </div>
                        <div class="card-body"> 
                                        
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                               <label class="col-form-label">Date d'operation : <strong class="text-success">{{ $dem->created_at }}</strong></label>
                                            </div>
                                        </div>
                                        
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                               <label class="col-form-label">Reçu numéro : <strong class="text-success">{{ $dem->recu }}</strong></label>
                                            </div>
                                        </div> 
                                        
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                               <label class="col-form-label">Mois : <strong class="text-success">{{ $dem->mois }}</strong></label>
                                            </div>
                                        </div> 
                                        
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                               <label class="col-form-label">Montant : <strong class="text-success">{{ $dem->montant }}</strong></label>
                                            </div>
                                        </div> 
                                        
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                               <label class="col-form-label">Mode de paiement : <strong class="text-success">{{ $dem->mode }}</strong></label>
                                            </div>
                                        </div> 
                                        
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                               <label for="exampleFormControlTextarea1"> <strong> Pièce justificative </strong> </label>
                                            </div>
                                        </div>
                                       
                                        <div class="form-group">
                                            <img src="{{ url($dem->piece) }}" class="img-fluid" width="400px">
                                        </div>
                                       
    
                        </div>
                    </div>
              
                </div>
    
            </div>
   

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

 <!-- Modal -->    


  @endsection

