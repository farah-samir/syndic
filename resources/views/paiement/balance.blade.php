@extends('layouts.master')

@section('content')



        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Balance</h1>
          </div>
            @if (session('status'))
                <p class="mt-3 mb-3 mb-0 alert alert-success">{{ session('status') }}</p>
            @endif
          <!-- Content Row -->
          <div class="row">

                <div class="col-lg-12">
              
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Balance</h6>
                        </div>
                        <div class="card-body">
    
                                <form class="needs-validation" action="{{ url('addBalance') }}" method="post">
                                    @csrf
                                    <div class="form-row">
        
                                        <div class="col-md-3 mb-3">
                                            <select class="form-control myselect" name="residt">
                                                <option disabled selected>Résidant</option>
                                                @foreach($resid as $resids) 
                                                    <option value="{{ $resids->id}}">{{ $resids->name}} {{ $resids->prenom }}</option>
                                                @endforeach
                                            </select>
        
                                            <script type="text/javascript">
                                                $(".myselect").select2();
                                            </script> 
        
                                            <div class="valid-tooltip">
                                                Looks good!
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3 mb-3">
                                            <input type="number" class="form-control" min="0" name="balance">    
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <button class="btn-shadow btn btn-info btn-sm" type="submit">Ajouter au balance</button>
                                        </div>
                                        
                                    </div>
                                </form>
    
                        </div>
                    </div>
              
                </div>
    
            </div>

            <!-- detail -->
   
            <div class="row">
                
            </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->



  @endsection

