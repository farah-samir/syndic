@extends('layouts.master')

@section('content')



        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Etat Paiement</h1>
          </div>
            
            @foreach($encour as $encours)
            <p class="mt-3 mb-3 mb-0 alert alert-danger">
                Paiement recu:
                <strong>
                
                    {{ $encours->recu}}
                
                </strong>
                est en cour de traitement.
            </p>
            @endforeach
            
          <!-- Content Row -->

            <div class="row">
                <div class="col-md-12">

                   
                        
                        <div class="row">
                            <div class="col-lg-12">
                          
                                <div class="card shadow mb-4">
                
                                    <div class="card-header py-3">
                                        

                                        <h6 class="m-0 font-weight-bold text-primary">Liste </h6>
                                    </div>
                                <form action="{{ url('paieFacture') }}" method="post">
                                    @csrf
                                    <div class="card-body">
                                        
                                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                        
                                        <button class="d-sm-inline-block btn btn-sm btn-primary shadow-sm float-right mr-2" type="submit">
                                    <i class="fas fa-address-card fa-sm text-white-50 mr-1"></i> Payer factures</button>
                
                                        <h5>Balance : <span class="badge badge-success">
                                            @if($b3 < 0)
                                            {{ 00 }}
                                            @else
                                            {{ $b3 }}
                                            @endif
                                            dh</span></h5>
                                        <h5>Rest à payer : <span class="badge badge-danger">{{ $somme }} dh</span></h5>
                                        
                                        <nav aria-label="..." class="float-right">
                                            <ul class="pagination">
                                                link
                                            </ul>
                                        </nav>
                
                                        <table class="table table-bordered table-responsive-lg">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Recu Num</th>
                                                    <th scope="col">mois</th>
                                                    <th scope="col">cotis</th>
                                                    <th scope="col">Etat</th>
                                                    <th scope="col">Reçu</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                @if( Auth::user()->solde != 0 )
                                                    <tr style="background-color:#f9eae8;">
                                                        <th colspan=5>Solde initial : 
                                                            <span class="text-danger"><strong>{{ Auth::user()->solde }} MAD</strong></span>
                                                        </th>
                                                        <th>
                                                            <button type="button" class="btn btn-info btn-sm mr-2">
                                                                Payer
                                                            </button>
                                                        </th>
                                                    </tr>
                                                @endif
                                                
                                                @foreach($paie as $paies)
                                                    <tr><th>
                                                            @if( $paies->etat == 'Impayé')
                                                                <div class="form-group form-check">
                                                                    <input type="checkbox" class="form-check-input" 
                                                                        value="{{ $paies->id }}" name="paie[]" >
                                                                </div>
                                                            @else
                                                                <div class="form-group form-check">
                                                                    <input type="checkbox" class="form-check-input" 
                                                                        disabled checked>
                                                                </div>
                                                            @endif
                                                            
                                                            
                                                        </th>
                                                        <th scope="row"><i class="fas fa-fw fa-folder"></i> {{ $paies->id }}</th>
                                                        <th scope="row">{{ $paies->mois }}</th>
                                                        <th scope="row">{{ $paies->cotisation }}</th>
                                                        <th scope="row"> 
                                                            @if( $paies->etat == 'Impayé')
                                                                <span class="text-danger">{{ $paies->etat }}</span>
                                                            @else
                                                                <span class="text-success">{{ $paies->etat }}</span>
                                                            @endif
                                                        </th>
                                                        <th scope="row">
                                                            @if( $paies->etat == 'Impayé')
                                                                <span class="text-danger">Reçu non Dispo</span>
                                                            @else
                                                                <span class="text-success">Reçu Dispo</span>
                                                            @endif
                                                        </th>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                
                                        <nav aria-label="..." class="mt-1 float-right">
                                            <ul class="pagination">
                                                link
                                            </ul>
                                        </nav>
                
                                    </div>
                                </form>
                                </div>
                          
                            </div>
                
                        </div>
             


                </div>
            </div>
   
            <div class="row">
                
            </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

  

  @endsection

